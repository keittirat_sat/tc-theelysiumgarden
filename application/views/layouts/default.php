<!DOCTYPE html>
<html lang="us">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo (empty($title)) ? "Trycatch™ | Mockup" : "{$title} | The Elysium Garden Limited Partnership"; ?></title>
        <?php echo css_asset("bootstrap.min.css"); ?>
        <?php echo css_asset("nivo-slider.css"); ?>
        <?php echo css_asset("colorbox.css"); ?>
        <?php echo css_asset("theelysium.css"); ?>
        <?php echo (empty($css)) ? "<!--No Additional CSS-->" : $css; ?>

        <?php echo js_asset("jquery-1.7.min.js"); ?>
        <?php echo js_asset("bootstrap.min.js"); ?>
        <?php echo js_asset("jquery.form.js"); ?>
        <?php echo js_asset("jquery.metadata.js"); ?>
        <?php echo js_asset("jquery.nivo.slider.pack.js"); ?>
        <?php echo js_asset("jquery.colorbox-min.js"); ?>
        <?php echo js_asset("jQueryRotateCompressed.js"); ?>
        <?php echo js_asset("jquery.color.js"); ?>
        <?php echo js_asset("trycatch_parallax.js"); ?>
        <?php echo (empty($js)) ? "<!--No Additional JS-->" : $js; ?>

        <script type="text/javascript">
            function quantity_control(param, operation) {
                var target = $(param).metadata().target;
                var current_amount = parseInt($('.' + target).val());
                var limit = parseInt($('.' + target).attr('max'));
                var aspect_val = 0;
                if (operation === "add") {
                    aspect_val = current_amount + 1;
                    if (aspect_val <= limit) {
                        $('.' + target).val(aspect_val);
                    } else {
                        $('.' + target).val(limit);
                    }
                } else {
                    aspect_val = current_amount - 1;
                    if (aspect_val >= 0) {
                        $('.' + target).val(aspect_val);
                    } else {
                        $('.' + target).val(0);
                    }
                }
            }
            $(function() {
                var limit = $('.slide').length;
                var index = 0;
                var current = $('.slide').first();
                var next_slide;

                $(current).css({'z-index': 2});

                setInterval(function() {
                    if ((index + 1) < limit) {
                        next_slide = $(current).next();
                        index++;
                    } else {
                        next_slide = $('.slide').first();
                        index = 0;
                    }

                    $(next_slide).css({'opacity': 1, 'z-index': 1});
                    $(current).animate({'opacity': 0}, 1500, function() {
                        $(next_slide).css({'z-index': 2});
                        current = next_slide;
                    });
                }, 10000);

                $('a.menu-element').mouseenter(function() {
                    //on hover
                    $(this).stop().animate({'color': '#39522b'}, 200);
                }).mouseleave(function() {
                    //on blur
                    $(this).stop().animate({'color': '#86bf60'}, 200);
                });

                $('.colorbox').colorbox({'maxWidth': '90%', 'maxHeight': '90%'});
                $('.set_popover').popover({'html': 'true', trigger: 'hover', placement: 'top'});

                $('.add_more_quantity').click(function() {
                    quantity_control(this, "add");
                });

                $('.de_more_quantity').click(function() {
                    quantity_control(this, "desc");
                });

                $('.number').blur(function() {
                    var number = $(this).val();
                    var target = $(this).attr('target');
                    if (number < 0) {
                        $('.' + target).val(0);
                    } else {
                        $('.' + target).val(number);
                    }
                    console.log(number);
                });
            });
        </script>
    </head>
    <body class="<?php echo ($this->auth->is_loged('admin')) ? "admin_loged" : ""; ?>">
        <?php if ($this->auth->is_loged('admin')): ?>
            <div class="admin-bar">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span3">
                            <a href="<?php echo site_url('trycatch/welcome'); ?>" class="btn btn-mini" style="display: block; margin-top: 5px;"><i class="icon icon-home"></i>&nbsp;The Elysiumgarden by Trycatch™</a>
                        </div>
                        <div class="span9" style="text-align:right;">
                            <?php $user = $this->auth->get_user_info(); ?>
                            <span style="margin-right: 10px;">
                                <i class="icon icon-white icon-user"></i>&nbsp;<?php echo $user->name; ?>
                            </span>
                            <a href="<?php echo site_url('api/logout'); ?>" class="btn btn-mini btn-danger"><i class="icon icon-white icon-share"></i>&nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                body.admin_loged{
                    margin-top: 32px;
                }
                .admin-bar {
                    width: 100%;
                    position: fixed;
                    top: 0px;
                    line-height: 31px;
                    background-color: #222;
                    color: #fff;
                    height: 32px;
                    z-index: 999;
                    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                    font-size: 14px;
                }
            </style>
        <?php endif; ?>
        <div class="slide_upper">
            <div class="slide" style="background-image: url('<?php echo image_asset_url("slide1.jpg"); ?>');"></div>
            <div class="slide" style="background-image: url('<?php echo image_asset_url("slide2.jpg"); ?>');"></div>
            <!--<div class="slide"></div>-->
            <div class="container" style="position: relative; z-index: 9;">
                <a href="<?php echo site_url(); ?>" class="logo span4"></a>
                <!--Lang bar-->
                <div class="lang-bar">
                    <a href="<?php echo site_url(); ?>" class="flag en-lang"></a>
                    <a href="<?php echo site_url('page/th/index'); ?>" class="flag th-lang"></a>
                </div>
                <!--End lang bar-->
            </div>
        </div>
        <div class="menu-bar">
            <div class="container">
                <div class="row-fluid" style="position: relative;">
                    <!--Normal menu-->
                    <a href="<?php echo site_url(($lang == "th") ? "page/th/index" : ""); ?>" class="menu-element diavlo">Home</a>
                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "product"); ?>" class="menu-element diavlo">Product</a>
                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "gardening"); ?>" class="menu-element diavlo">Gardening</a>
                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "faq"); ?>" class="menu-element diavlo">FAQ</a>
                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "contact"); ?>" class="menu-element diavlo">Contact Us</a>
                    <!--End normal menu-->

                    <?php if ($loged && empty($transaction_id)): ?>
                        <a href="#cart" class="cart_btn">
                            <span>Order <b id="total_order">(<?php echo count($cart); ?>)</b></span>
                        </a>
                    <?php endif; ?>

                    <!--User Gateway-->
                    <div class="user_gateway_box">
                        <?php if ($loged): ?>
                            <!--Login state-->
                            <span class="name_login loged label label-success">Welcome, <?php echo $user->name; ?></span>
                            <a href="#account" class="btn diavlo" role="button" data-toggle="modal">ACCOUNT</a>
                            <a href="<?php echo site_url('api/logout'); ?>" class="btn btn-danger diavlo" role="button">LOGOUT</a>
                            <!--End login state-->
                        <?php else: ?>
                            <!--Guest state-->
                            <span class="name_login">Hi, Guest</span>
                            <a href="#login" class="btn btn-success diavlo" role="button" data-toggle="modal">LOGIN</a>
                            <a href="#signup" class="btn diavlo" role="button" data-toggle="modal">SIGN UP</a>
                            <!--end guest state-->
                        <?php endif; ?>
                    </div>
                    <!--End User Gateway-->
                    <?php if ($loged && empty($transaction_id)): ?>
                        <div class="cart-sheet span7 offset2">
                            <div class="order-list-title"></div>
                            <div style="padding: 10px;">
                                <table class="table">
                                    <thead>
                                        <tr style="color: #222; font-size: 15px;">
                                            <th style="text-align:center;">Cat</th>
                                            <th style="text-align:center;">Item</th>
                                            <th style="text-align:center;">Unit</th>
                                            <th style="text-align:center;">Price</th>
                                            <th style="text-align:center;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cart_list">
                                        <?php $total = 0; ?>
                                        <?php foreach ($cart as $index => $product): ?>
                                            <?php switch ($product['category']): case 'fruit': ?>
                                                    <?php $color = "rgba(255,204,0,0.25)"; ?>
                                                    <?php break; ?>
                                                <?php case 'vagetable': ?>
                                                    <?php $color = "rgba(153,255,51,0.25)"; ?>
                                                    <?php break; ?>
                                                <?php case 'flower': ?>
                                                    <?php $color = "rgba(255,102,204,0.25)"; ?>
                                                    <?php break; ?>
                                            <?php endswitch; ?>

                                            <tr style="color: #222; font-size: 15px; background-color: <?php echo $color; ?>;">
                                                <td><?php echo $product['category']; ?></td>
                                                <td style="text-align: left; "><?php echo ($lang == 'en') ? $product['name']->en : $product['name']->th; ?></td>
                                                <td style="text-align: right;"><?php echo $product['priece']; ?></td>
                                                <td style="text-align: right;"><?php echo $product['price']; ?></td>
                                                <td style="text-align: right;"><?php echo $product['total']; ?> THB.</td>
                                            </tr>
                                            <?php $total += $product['total']; ?>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td colspan="4" style="text-align: right; font-size: 15px; color: #99cc66;"><b>Total price</b></td>
                                            <td style="text-align: right;"><?php echo "{$total}" ?><span style="font-size: 15px; line-height: 20px;"> THB.</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="txt_right">
                                    <a href="<?php echo site_url("page/checkout/" . do_hash($user->uid . date('U'))); ?>#onion" class="checkout_btn"></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php echo $contents; ?>
        <div class="footer">
            <div style="width: 510px; margin: 0px auto;">
                <div class="footer-image" style="display: inline-block"></div>
                <div class="diavlo font-txt" style="display: inline-block">
                    <h3 style="line-height: 30px;">The Elysium Garden<br/>Limited Partnership</h3>
                    <?php $y = date('Y'); ?>
                    <p>&COPY; <?php echo ($y == 2012) ? "2012" : "2012-{$y}"; ?> <a href="<?php echo site_url(); ?>">The Elysium Garden</a> by <a href="http://www.trycatch.in.th">TryCatch&trade;</a></p>
                </div>
            </div>
        </div>
        <!--Medal Section-->


        <?php if ($loged): ?>
            <!--Already Login-->
            <div class="notification">
                <p>Added !!!</p>
            </div>
            <div id="account" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel" class="diavlo">Account management</h3>
                </div>
                <div class="modal-body">
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">Profile</a>
                            </li>
                            <li><a href="#tab2" data-toggle="tab">Order history</a></li>
                            <li><a href="#tab3" data-toggle="tab">Change Password</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <!--Edit profile-->
                                <div class="row-fluid">
                                    <form class="margin-bottom-none" id="form_user_update" method="post" action="<?php echo site_url("api/update_profile/{$user->uid}"); ?>">
                                        <div class="control-group">
                                            <label class="control-label" for="inputName">Name / Company</label>
                                            <div class="controls">
                                                <input type="text" id="inputName" placeholder="Name / Company" name='name' class="span12" value="<?php echo $user->name; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputUsername">Username</label>
                                            <div class="controls">
                                                <input type="text" id="inputUsername" placeholder="Username" name='username' class="span12" value="<?php echo $user->username; ?>" disabled>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputemail">Email</label>
                                            <div class="controls">
                                                <input type="email" id="inputemail" placeholder="Email" name='email' class="span12" value="<?php echo $user->email; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputtelephone">Telephone</label>
                                            <div class="controls">
                                                <input type="text" id="inputtelephone" placeholder="Telephone" name='telephone' class="span12" value="<?php echo $user->telephone; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputtelephone">Fax</label>
                                            <div class="controls">
                                                <input type="text" id="inputtelephone" placeholder="Fax" name='fax' class="span12" value="<?php echo $user->fax; ?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputAddress1">Address 1</label>
                                            <div class="controls">
                                                <input type="text" id="inputAddress1" placeholder="Address line1" name='address1' class="span12" value="<?php echo $user->address1; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputAddress2">Address 2</label>
                                            <div class="controls">
                                                <input type="text" id="inputAddress2" placeholder="Address line2" name='address2' class="span12" value="<?php echo $user->address2; ?>">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputDistrict">District</label>
                                            <div class="controls">
                                                <input type="text" id="inputDistrict" placeholder="District" name='district' class="span12" value="<?php echo $user->district; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="inputProvince">Province</label>
                                            <div class="controls">
                                                <input type="text" id="inputProvince" placeholder="Province" name='province' class="span12" value="<?php echo $user->province; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group margin-bottom-none">
                                            <label class="control-label" for="inputZip">Zip code</label>
                                            <div class="controls">
                                                <input type="text" id="inputZip" placeholder="#ZIP" name='zip' class="span12" value="<?php echo $user->zip; ?>" required>
                                            </div>
                                        </div>
                                        <div class="control-group margin-bottom-none">
                                            <div class="controls" style="text-align: right;">
                                                <button class="btn btn-primary" type='submit'>UPDATE</button>
                                                <button class="btn btn-danger" type='reset'>RESET</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <?php $my_order = $this->getdata->get_order_by_user_id($user->uid, 'desc'); ?>
                                <?php if (!empty($my_order)): ?>
                                    <div class="accordion" id="order_collapse">
                                        <?php foreach ($my_order as $key => $each_order): ?>
                                            <?php switch ($each_order->status): case 0: ?>
                                                    <?php $mode = "[New order]"; ?>
                                                    <?php $color = "#efefef"; ?>
                                                    <?php break; ?>
                                                <?php case 1: ?>
                                                    <?php $mode = "[Processing]"; ?>  
                                                    <?php $color = "#fffabe"; ?>                                                              
                                                    <?php break; ?>
                                                <?php case 2: ?>
                                                    <?php $mode = "[Credit]"; ?>   
                                                    <?php $color = "#ffbebe"; ?>                                                             
                                                    <?php break; ?>
                                                <?php case 3: ?>
                                                    <?php $mode = "[Paid]"; ?>  
                                                    <?php $color = "#beffcc"; ?>                                                              
                                                    <?php break; ?>
                                            <?php endswitch; ?>
                                            <div class="accordion-group">
                                                <div class="accordion-heading" style="background-color:<?php echo $color; ?>">
                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#order_collapse" href="<?php echo "#order{$each_order->order_id}"; ?>">
                                                        <?php echo $this->convert_id->order_id($each_order->order_id, $each_order->order_date) . " " . $mode; ?>
                                                    </a>
                                                </div>
                                                <div id="<?php echo "order{$each_order->order_id}"; ?>" class="accordion-body collapse <?php echo ($key == 0) ? "in" : ""; ?>">
                                                    <div class="accordion-inner">
                                                        <p><b>Order:</b> <?php echo date('d F Y', $each_order->order_date); ?></p>
                                                        <?php if ($each_order->transport_date > 0): ?>
                                                            <p><b>Transfer:</b> <?php echo date('d F Y', $each_order->transport_date); ?></p>
                                                        <?php endif; ?>
                                                        <?php if ($each_order->credit_date > 0): ?>
                                                            <p><b>Due date:</b> <?php echo date('d F Y', $each_order->credit_date); ?></p>
                                                        <?php endif; ?>
                                                        <table class='table table-striped'>
                                                            <tr>
                                                                <th class='txt_center'>#</th>
                                                                <th class='txt_center'>Product</th>
                                                                <th class='txt_center'>Quantity</th>
                                                            </tr>
                                                            <?php foreach ($each_order->order_item as $kindex => $product): ?>
                                                                <?php $product_name = json_decode($product->product_detail->pname); ?>
                                                                <tr>
                                                                    <td class='txt_center'><?php echo $kindex + 1; ?></td>
                                                                    <td class='txt_left'><?php echo ($lang == 'en') ? $product_name->en : $product_name->th; ?></td>
                                                                    <td class='txt_center'><?php echo $product->amount; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php else: ?>
                                    <h4 style="text-align: center;" class="diavlo">--- Not found order history ---</h4>
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <form action='<?php echo site_url('api/chage_pass'); ?>' method='post' id='change_password' class='row-fluid'>
                                    <div class="control-group">
                                        <label>Current password</label>
                                        <input type='password' name='password' placeholder='Current password' class='span12' autocomplete="off" required>
                                    </div>
                                    <div class="control-group password_section">
                                        <label class='control-label'>New password</label>
                                        <input type='password' id='new_password' name='new_password' placeholder='New password' class='span12' autocomplete="off" required>
                                    </div>
                                    <div class="control-group password_section">
                                        <input type='password' id='new_password2' name='new_password2' placeholder='Retype new password' class='span12' autocomplete="off" required>
                                    </div>
                                    <div class="control-group">
                                        <button class='btn btn-danger' type='reset'>RESET</button>
                                        <button class='btn btn-primary' type='button' id='btn_chang_btn'>CHANGE PASSWORD</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CLOSE</button>
                </div>
            </div>
            <script type="text/javascript">
                $(function() {

                    //Add Cart
                    $('.add_cart_btn').click(function() {
                        var target = $(this).metadata().target;
                        var pid = $(this).metadata().pid;
                        var total = $('.' + target).val();
                        total = parseInt(total);
                        //console.log(target, pid, total);
                        if (total > 0) {
                            var btn = $(this);
                            btn.button('loading');
                            $.post('<?php echo site_url('cart/add_cart'); ?>', {pid: pid, total: total}, function(res) {
                                console.log(res);
                                var total_order = res.total_type_of_product;
                                $('#total_order').text("(" + total_order + ")");
                                $('.notification').stop().animate({top: 0}, 500, function() {
                                    setTimeout(function() {
                                        $('.notification').stop().animate({top: '-50'}, 500);
                                        btn.button('reset');
                                    }, 500);
                                });
                                $.get('<?php echo site_url('page/cart_template'); ?>', function(res) {
                                    $('#cart_list').html(res);
                                });
                            }, 'json');
                        } else {
                            alert('จำนวนขั้นต่ำในการสั่งซื้อคือ 1 ชิ้นต่อ 1 ผลิตภัณฑ์');
                        }
                    });

                    //check password

                    var change_password_state = false;
                    $('#new_password2').keyup(function() {
                        var password_single = $('#new_password2').val();
                        var current = $('#new_password').val();
                        if (password_single === current) {
                            change_password_state = true;
                            $('.password_section').removeClass('error');
                        } else {
                            change_password_state = false;
                            $('.password_section').addClass('error');
                        }
                    });

                    $('#btn_chang_btn').click(function() {
                        if (change_password_state) {
                            $('form#change_password').submit();
                        } else {
                            alert('Password mismatch');
                        }
                    });

                    $('form#change_password').ajaxForm({
                        complete: function(res) {
                            var response = $.parseJSON(res.responseText);
                            console.log(response);
                            if (response.status === 'success') {
                                alert('Please login !!!');
                                location.reload();
                            } else {
                                alert('Error');
                            }
                        }
                    });

                    //Update user profile
                    $('form#form_user_update').ajaxForm({
                        complete: function(response) {
                            var state = $.parseJSON(response.responseText);
                            if (state.status === "success") {
                                location.reload();
                            } else {
                                alert("Error");
                            }
                        }
                    });

                    $('.checkout_btn').mousedown(function() {
                        $(this).addClass('active');
                    }).mouseup(function() {
                        $(this).removeClass('active');
                    });

                    $('.cart-sheet').animate({height: "toggle", opacity: 'toggle'}, 0);
                    $('.cart_btn').click(function() {
                        $('.cart-sheet').animate({height: "toggle", opacity: 'toggle'}, 400, "linear");
                    });

                });
            </script>
        <?php else: ?>
            <!--Guest Mode-->
            <!--Medal for login-->
            <div id="login" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel" class="diavlo">Welcome to The Elysium Garden</h3>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id='alert_username_mismatch'>
                        Username or Password isn't correct
                    </div>
                    <div class="alert" id='alert_username_blank'>
                        Username and Password is required fields cannot leave blank.
                    </div>
                    <form class="form-horizontal margin-bottom-none" id="form_login" method="post" action="<?php echo site_url('api/guest_login'); ?>">
                        <div class="control-group">
                            <label class="control-label" for="inputUsername">Username</label>
                            <div class="controls">
                                <input type="text" id="inputUsername" placeholder="Username" name='username'>
                            </div>
                        </div>
                        <div class="control-group margin-bottom-none">
                            <label class="control-label" for="inputPassword">Password</label>
                            <div class="controls">
                                <input type="password" id="inputPassword" placeholder="Password" name='password'>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">CLOSE</button>
                    <button class="btn btn-success" id="form_login_submit" data-loading-text="Logging in...">LOGIN</button>
                </div>
            </div>

            <!--Medal for Signup-->
            <div id="signup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Please fill all required fields</h3>
                </div>
                <div class="modal-body">
                    <div class='alert alert-info'>
                        <h4>Please note !!</h4>
                        After submit this form. we will review your request before allow your account online.
                    </div>
                    <div class='alert alert-danger' id='pass_mismatch'>
                        <b>Error</b> Password mismatch.
                    </div>
                    <div class='alert alert-danger' id='field_miss'>
                        <b>Error</b> Please check all required fields.
                    </div>
                    <form class="form-horizontal margin-bottom-none" id="form_regist" method="post" action="<?php echo site_url('api/regist'); ?>">
                        <div class="control-group">
                            <label class="control-label" for="inputName">Name / Company</label>
                            <div class="controls">
                                <input type="text" id="inputName" placeholder="Name / Company" name='name' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputemail">Email</label>
                            <div class="controls">
                                <input type="email" id="inputemail" placeholder="Email" name='email' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputtelephone">Telephone</label>
                            <div class="controls">
                                <input type="text" id="inputtelephone" placeholder="Telephone" name='telephone' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputtelephone">Fax</label>
                            <div class="controls">
                                <input type="text" id="inputtelephone" placeholder="Fax" name='fax'>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputuser">Username</label>
                            <div class="controls">
                                <input type="text" id="inputuser" placeholder="Username" name='username' required>
                            </div>
                        </div>
                        <div class="control-group" id='parent_pass1'>
                            <label class="control-label" for="inputpass">Password</label>
                            <div class="controls">
                                <input type="password" id="inputpass" placeholder="Password" name='password' required>
                            </div>
                        </div>
                        <div class="control-group" id='parent_pass2'>
                            <label class="control-label" for="inputpass2">Password twice</label>
                            <div class="controls">
                                <input type="password" id="inputpass2" placeholder="Password twice" name='password2' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputAddress1">Address 1</label>
                            <div class="controls">
                                <input type="text" id="inputAddress1" placeholder="Address line1" name='address1' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputAddress2">Address 2</label>
                            <div class="controls">
                                <input type="text" id="inputAddress2" placeholder="Address line2" name='address2'>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputDistrict">District</label>
                            <div class="controls">
                                <input type="text" id="inputDistrict" placeholder="District" name='district' required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="inputProvince">Province</label>
                            <div class="controls">
                                <input type="text" id="inputProvince" placeholder="Province" name='province' required>
                            </div>
                        </div>
                        <div class="control-group margin-bottom-none">
                            <label class="control-label" for="inputZip">Zip code</label>
                            <div class="controls">
                                <input type="text" id="inputZip" placeholder="#ZIP" name='zip' required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" id='submit_regist' data-loading-text="Sending ...">SUBMIT</button>
                    <button class="btn btn-danger" id='reset_regist'>RESET</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true" id='close_btn_regist'>CLOSE</button>
                </div>
            </div>

            <script type="text/javascript">
                $(function() {

                    $('#submit_regist').button();

                    $('#alert_username_mismatch').hide();
                    $('#alert_username_blank').hide();

                    $('#field_miss').hide();
                    $('#pass_mismatch').hide();

                    $('#form_login_submit').click(function() {
                        $('#form_login').submit();
                    });

                    $('#form_login').submit(function() {
                        var username = $('[name=username]').val();
                        var password = $('[name=password]').val();
                        var f = true;

                        if (!username) {
                            f = false;
                        }

                        if (!password) {
                            f = false;
                        }

                        if (!f) {
                            $('#alert_username_blank').show();
                            return false;
                        }
                        return true;
                    });

                    $('#form_login').ajaxForm({
                        beforeSend: function() {
                            $('#alert_username_mismatch').hide();
                            $('#alert_username_blank').hide();
                            $('#form_login_submit').button('loading');
                        },
                        complete: function(xhr) {
                            var res = $.parseJSON(xhr.responseText);
                            if (res.status === "success") {
                                location.reload();
                            } else {
                                console.log(res);
                                $('#form_login').trigger('reset');
                                $('#alert_username_mismatch').show();
                            }
                            $('#form_login_submit').button('reset');
                        }
                    });

                    $('#submit_regist').click(function() {
                        $('#form_regist').submit();
                    });

                    $('#reset_regist').click(function() {
                        $('#form_regist').trigger('reset');
                    });

                    $('#form_regist').submit(function() {

                        $('#parent_pass1').removeClass('error');
                        $('#parent_pass2').removeClass('error');

                        $('#field_miss').hide();
                        $('#pass_mismatch').hide();

                        var name = $('[name=name]').val();
                        var email = $('[name=email]').val();
                        var username = $('#inputuser').val();
                        var pass1 = $('#inputpass').val();
                        var pass2 = $('#inputpass2').val();
                        var address1 = $('[name=address1]').val();
                        var district = $('[name=district]').val();
                        var province = $('[name=province]').val();
                        var zip = $('[name=zip]').val();

                        var f = true;

                        if (!name) {
                            f = false;
                        }

                        if (!email) {
                            f = false;
                        }

                        if (!username) {
                            f = false;
                        }

                        if (!pass1) {
                            f = false;
                        }

                        if (!pass2) {
                            f = false;
                        }

                        if (!address1) {
                            f = false;
                        }

                        if (!address1) {
                            f = false;
                        }

                        if (!district) {
                            f = false;
                        }

                        if (!province) {
                            f = false;
                        }

                        if (!zip) {
                            f = false;
                        }

                        if (pass1 !== pass2) {
                            f = false;
                            $('#parent_pass1').addClass('error');
                            $('#parent_pass2').addClass('error');
                            $('#pass_mismatch').show();
                            console.log(pass1, pass2);
                        }

                        if (f) {
                            return true;
                        }
                        $('#field_miss').show();
                        return false;
                    });

                    $('#form_regist').ajaxForm({
                        beforeSend: function() {
                            $('#submit_regist').button('loading');
                        },
                        complete: function(xhr) {
                            var json = $.parseJSON(xhr.responseText);
                            if (json.status === 'success') {
                                $('#close_btn_regist').trigger('click');
                                alert('Thank you for register');
                            } else {
                                alert(json.response);
                            }
                            $('#submit_regist').button('reset');
                        }
                    });
    <?php $state = $this->input->get('approve'); ?>
    <?php if ($state): ?>
        <?php if ($state == 'success'): ?>
                            alert('Member approved');
        <?php else: ?>
                            alert('Cannot approved :(');
        <?php endif; ?>
    <?php endif; ?>
                });
            </script>
        <?php endif; ?>
        <!--End Medal section-->

        <!-- Google Analytic -->
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-42013443-7', 'theelysiumgarden.com');
            ga('send', 'pageview');

        </script>
    </body>
</html>
