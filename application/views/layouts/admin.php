<!DOCTYPE html>
<html lang="us">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo (empty($title)) ? "Trycatch™ | Management System" : "{$title} | Trycatch™ &raquo; Contents Management System"; ?></title>
        <?php echo css_asset("bootstrap.min.css"); ?>
        <?php echo css_asset("datepicker.css"); ?>
        <?php echo css_asset("cms.css"); ?>
        <?php echo (empty($css)) ? "<!--No Additional CSS-->" : $css; ?>

        <?php echo js_asset("jquery-1.7.min.js"); ?>
        <?php echo js_asset("bootstrap.min.js"); ?>
        <?php echo js_asset("jquery.form.js"); ?>
        <?php echo js_asset("jquery.metadata.js"); ?>
        <?php echo js_asset("bootstrap-datepicker.js"); ?>
        <?php echo (empty($js)) ? "<!--No Additional JS-->" : $js; ?>
    </head>
    <body class="<?php echo ($this->auth->is_loged()) ? "admin_loged" : ""; ?>">
        <?php if ($this->auth->is_loged('admin')): ?>
            <div class="admin-bar">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span3">
                            <a href="<?php echo site_url(); ?>" class="btn btn-mini" style="display: block; margin-top: 5px;"><i class="icon icon-home"></i>&nbsp;The Elysiumgarden by Trycatch™</a>
                        </div>
                        <div class="span9" style="text-align:right;">
                            <?php $user = $this->auth->get_user_info(); ?>
                            <span style="margin-right: 10px;">
                                <i class="icon icon-white icon-user"></i>&nbsp;<?php echo $user->name; ?>
                            </span>
                            <a href="<?php echo site_url('api/logout'); ?>" class="btn btn-mini btn-danger"><i class="icon icon-white icon-share"></i>&nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="container">
            <div <?php echo ($this->auth->is_loged('admin')) ? "class='row-fluid'" : ""; ?>>
                <?php echo (empty($sidebar)) ? "<!-- No Sidebar create -->" : $sidebar; ?>
                <?php echo $contents; ?>
            </div>
        </div>
    </body>
</html>
