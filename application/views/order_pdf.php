<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Order Template</title>
    </head>
    <body>
        <!-- 6 Col -->
        <?php $vat = $this->getdata->get_vat(); ?>
        <table style="width:100%; border-collapse: collapse; font-family: sarabun; font-size: 9pt; line-height: 1.8;">
            <thead>
                <tr>
                    <th style="text-align: left; padding-bottom: 30px;">
                        <img src="/application/assets/image/logo_reactant.png" title="logo" style="width: 120px; height: auto; padding: 0px;">
                    </th>
                    <th colspan="5" style="text-align: left; vertical-align: top; font-weight: normal; font-size: 12px; line-height: 20px; padding: 0px 5px 45px;">
                        <b style="font-size: 15px;">ห้างหุ้นส่วนจำกัด ดิอีลิซเซียม การ์เด้น (Theelysiumgarden Limited partnership)</b>
                        <br/>เลขที่ 153 หมู่ 1 ต.สันทรายหลวง อ.สันทราย จ.เชียงใหม่ 50210 
                        <br/><b>โทร</b> 084-3671262, 083-3228008 , 075-640044 โทรสาร 075-640044 
                        <br/><b>E-mail:</b> theelysiumgarden@gmail.com <b>Website:</b> www.theelysiumgarden.com
                        <br/><b>ทะเบียนเลขที่</b> 0503556003860 <b>เลขประจำตัวผู้เสียภาษี</b> 1819900007272
                    </th>
                </tr>
                <tr style=" background-image: linear-gradient(rgba(255, 255, 255, 1), rgba(135, 195, 99, 0.5) 100%);">
                    <th colspan="6" style="text-align: center; padding: 10px 2px 5px; font-size: 18px;"><b>ใบสั่งซื้อ / Purchase order</b></th>
                </tr>
                <tr>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">รหัสลูกค้า</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;">
                        <?php echo $this->convert_id->user_id($order->uid); ?>
                    </td>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">เลขที่</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;">
                        <?php echo $this->convert_id->order_id($order->order_id, $order->order_date); ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">ชื่อลูกค้า</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;"><?php echo $order->name; ?></td>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">วันที่</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;"><?php echo date('d F Y', $order->order_date); ?></td>
                </tr>
                <tr>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000; border-bottom: 0px;">ที่อยู่</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000; border-bottom: 0px;"><?php echo $order->address1 . " " . $order->address2 . " " . $order->district; ?></td>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">จัดส่งวันที่</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;"><?php echo ($order->transport_date == 0) ? "กำลังดำเนินการ ..." : date('d F Y', $order->transport_date); ?></td>
                </tr>
                <tr>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000; border-top: none; border-bottom: none;">&nbsp;</th>
                    <td colspan="2" style="padding: 3px 2px;"><?php echo $order->province . " " . $order->zip ?></td>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">จำนวนเครดิต</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;"><?php echo ($order->transport_date > 0) ? $this->convert_id->cal_date_diff($order->transport_date, $order->credit_date) . " วัน" : "รอการติดต่อจากเรา"; ?></td>
                </tr>
                <tr>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000; border-top: 0px;">&nbsp;</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000; border-top: none;"><?php echo "โทรศัพท์ {$order_user[0]->telephone} " . ((empty($order_user[0]->fax)) ? "" : "โทรสาร {$order_user[0]->fax}<br/>") . " อีเมลล์ {$order_user[0]->email}" ?></td>
                    <th style="text-align: left; padding: 3px 2px; border: 1px solid #000;">กำหนดชำระ</th>
                    <td colspan="2" style="padding: 3px 2px; border: 1px solid #000;"><?php echo ($order->credit_date == 0) ? "กำลังดำเนินการ ..." : date('d F Y', $order->credit_date); ?></td>
                </tr>
                <tr style=" background-image: linear-gradient(rgba(255, 255, 255, 1), rgba(135, 195, 99, 0.5) 100%);">
                    <td style="width:15%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">ลำดับ</td>
                    <td style="width:15%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">รหัส</td>
                    <td style="width:40%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">รายละเอียด</td>
                    <td style="width:10%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">ราคา/หน่วย</td>
                    <td style="width:10%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">จำนวนชิ้น</td>
                    <td style="width:10%; text-align: center; padding: 10px 2px 5px; font-weight: bold; border: 1px solid #000;">จำนวนเงิน</td>
                </tr>
            </thead>
            <tbody>
                <?php $all = 0; ?>
                <?php $limit = count($order_item) - 1; ?>
                <?php foreach ($order_item as $key => $item): ?>
                    <tr>
                        <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;"><?php echo $key + 1; ?></td>
                        <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">
                            <?php echo $this->convert_id->product_id($item->pid, $item->product_detail->category); ?>
                        </td>
                        <td style="width:40%; padding: 3px 2px; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">
                            <?php $name = json_decode($item->product_detail->pname) ?>
                            <?php echo $name->th; ?>
                        </td>
                        <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">
                            ฿<?php echo $item->product_detail->price; ?>
                        </td>
                        <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">
                            <?php echo $item->amount; ?>
                        </td>
                        <td style="width:10%; padding: 3px 2px; text-align: right; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">
                            <?php $price = $item->amount * $item->product_detail->price; ?>
                            ฿<?php echo $price; ?>
                            <?php $all += $price; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php $cal_line_space = $key % 10; ?>
                <?php for ($i = $cal_line_space; $i < 10; $i++): ?>
                    <tr>
                        <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                        <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                        <td style="width:40%; padding: 3px 2px; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                        <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                        <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                        <td style="width:10%; padding: 3px 2px; text-align: right; border: 1px solid #000; border-top: 0px; border-bottom: 0px;">&nbsp;</td>
                    </tr>
                <?php endfor; ?>
                <tr>
                    <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                    <td style="width:15%; padding: 3px 2px; text-align: center; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                    <td style="width:40%; padding: 3px 2px; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                    <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                    <td style="width:10%; padding: 3px 2px; text-align: center; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                    <td style="width:10%; padding: 3px 2px; text-align: right; border: 1px solid #000;  border-top: 0px;">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="padding: 3px 2px;">&nbsp;</td>
                    <td colspan="2" style="padding: 3px 2px; text-align: right; border: 1px solid #000;">ภาษีมูลค่าเพิ่ม <?php echo "{$vat}%"; ?></td>
                    <td style="padding: 3px 2px; text-align: right; border: 1px solid #000;">
                        <?php $cal_vat = $all * ($vat / 100); ?>
                        <?php echo "฿" . $cal_vat; ?>
                    </td>
                </tr> 
                <tr>
                    <td colspan="3" style="padding: 3px 2px;">&nbsp;</td>
                    <td colspan="2" style="padding: 3px 2px; text-align: right; border: 1px solid #000;">รวมทั้งสิ้น</td>
                    <td style="padding: 3px 2px; text-align: right; border: 1px solid #000;"><?php echo "฿" . ($all + $cal_vat); ?></td>
                </tr>                
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" style="padding: 20px 0px; vertical-align: top;">
                        <h3>ข้อกำหนดและเงื่อนไขการขอใบสั่งซื้อ</h3>
                        <ol>
                            <li>โปรดระบุเลขที่ใบสั่งซื้อข้างต้นในใบส่งของทุกฉบับ</li>
                            <li>การวางบิลและการรับเช็คเป็นไปตามกำหนดเวลาที่ห้างหุ้นส่วนจำกัดกำหนดไว้</li>
                        </ol>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="vertical-align: top; text-align: center; margin-top: 80px;">
                        <table style='width: 700px;'>
                            <tbody>
                                <tr>
                                    <th style='width: 350px;'>ผู้ตรวจสอบ</th>
                                    <th style='width: 350px;'>ผู้มีอำนาจลงนาม</th>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <th>______________________</th>
                                    <th>______________________</th>
                                </tr>
                                <tr>
                                    <th>( ______________________ )</th>
                                    <th>( ______________________ )</th>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>