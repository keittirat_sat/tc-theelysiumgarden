<div class="container" style="margin: 50px auto 20px;">
    <div class="row-fluid">
        <div class="span9">
            <div class="row-fluid show_room vegetable_section">    
                <!--Vegetable-->
                <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Vegetable</span></h1>
                <div class="row-fluid">
                    <?php foreach ($vegetable as $key => $rec): ?>
                        <?php $this->block_generator->get_product_from($rec, 4, $key, $lang); ?>
                    <?php endforeach; ?>
                </div>
                <!--end vegetable-->
            </div>
            <div class="row-fluid show_room flower_section">
                <!--Flower-->
                <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Flower</span></h1>
                <div class="row-fluid">
                    <?php foreach ($flower as $key => $rec): ?>                        
                        <?php $this->block_generator->get_product_from($rec, 4, $key, $lang); ?>
                    <?php endforeach; ?>
                </div>
                <!--end flower-->
            </div>
            <div class="row-fluid show_room fruit_section">
                <!--Fruit-->
                <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Fruit</span></h1>
                <div class="row-fluid">
                    <?php foreach ($fruit as $key => $rec): ?>
                        <?php $this->block_generator->get_product_from($rec, 4, $key, $lang); ?>
                    <?php endforeach; ?>
                </div>
                <!--end fruit-->
            </div>
        </div>
        <div class="span3">
            <a href="#vegetable" class="cate_product" style="margin-top: 20px;" for="vegetable_section">
                <i class="trycatch_icon vegetable"></i>
                <span class="diavlo white">Vegetable</span>
            </a>
            <a href="#flower" class="cate_product " for="flower_section">
                <i class="trycatch_icon flower"></i>
                <span class="diavlo white">Flower</span>
            </a>
            <a href="#fruit" class="cate_product " for="fruit_section">
                <i class="trycatch_icon fruit"></i>
                <span class="diavlo white">Fruit</span>
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('a.cate_product').click(function() {
            $('.show_room').hide();
            var id = $(this).attr('for');
            $('.' + id).show();
            $('a.cate_product').removeClass('active');
            $(this).addClass('active');
        }).first().trigger('click');
    });
</script>