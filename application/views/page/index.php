<div class="container">
    <div class="index_top_gradient"></div>
    <div class="row-fluid">
        <div class="span12">
            <h1 class="what_we diavlo">What we do?</h1>
        </div>
    </div>

    <div class="row-fluid white">
        <div class="span3 txt_right">
            <h3 class="diavlo" style="padding-top: 50px;">Vegetable</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh mauris, venenatis sit amet venenatis vel, molestie ut sem. Mauris ut tellus in purus cursus mollis. Vivamus rhoncus augue ac pulvinar rutrum.</p>
        </div>
        <div class="span6 shell_logo">
            <div class="shell_left"></div>
            <div class="shell_right"></div>
        </div>
        <div class="span3 txt_left">
            <h3 class="diavlo" style="padding-top: 50px;">Gardening</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh mauris, venenatis sit amet venenatis vel, molestie ut sem. Mauris ut tellus in purus cursus mollis. Vivamus rhoncus augue ac pulvinar rutrum.</p>
        </div>
    </div>

    <!--Start sample product-->
    <div class="row-fluid" style="position: relative; padding-bottom: 20px;">
        <!--Box Category Selection-->
        <div class="index_filter {index:0}">
            <a href="#prev" class="mode_prev"></a>
            <div class="display_switch"></div>
            <a href="#next" class="mode_next"></a>
        </div>
        <!--End Box Selection-->

        <div class="show_room vegetable_section">
            <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Vegetable</span></h1>
            <div class="row-fluid">
                <!--Vegetable-->
                <?php foreach ($vagetable as $key=> $rec): ?>
                    <?php $this->block_generator->get_product_from($rec, 3, $key); ?>
                <?php endforeach; ?>
                <!--End Vegetable-->
            </div>
        </div>

        <div class="show_room flower_section">
            <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Flower</span></h1>
            <div class="row-fluid">
                <!--fruit-->
                <?php foreach ($fruit as $key => $rec): ?>
                    <?php $this->block_generator->get_product_from($rec, 3, $key, $lang); ?>
                <?php endforeach; ?>
                <!--End fruit-->
            </div>
        </div>

        <div class="show_room fruit_section">
            <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">Fruit</span></h1>
            <div class="row-fluid">
                <!--flower-->
                <?php foreach ($flower as $key => $rec): ?>
                    <?php $this->block_generator->get_product_from($rec, 3, $key, $lang); ?>
                <?php endforeach; ?>
                <!--end flower-->
            </div>
        </div>

        <div class="show_room all_section">
            <h1 class=" white diavlo ">Product<span class="span_block_title_index diavlo_light">All Entry</span></h1>
            <div class="row-fluid">
                <!--All section-->
                <?php foreach ($all_product as $key => $rec): ?>
                    <?php $this->block_generator->get_product_from($rec, 3, $key, $lang); ?>
                <?php endforeach; ?>
                <!--End all section-->
            </div>
        </div>
    </div>    
    <!--End Sample Product-->
</div>

<div class="gardening-section parallax_section">
    <div class="container">
        <div class="row-fluid">
            <div class="span12" style="height: 450px;">
                <h1 class="white txt_center diavlo title-garden-index">Gardening</h1>
                <div class="span6 offset3 txt_center">
                    <p class="white" style="margin-bottom: 20px;">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh mauris, venenatis sit amet venenatis vel, molestie ut sem. Mauris ut tellus in purus cursus mollis. Vivamus rhoncus augue ac pulvinar rutrum. Aenean vehicula.
                    </p>

                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "gardening"); ?>" class="check_me_out_graden"></a>
                    <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "gardening"); ?>" class="white cmo">Find out more</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row-fluid">
        <div class="span10 offset1 txt_center white">
            <div class="footer-detail_garden">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh mauris, venenatis sit amet venenatis vel, molestie ut sem. Mauris ut tellus in purus cursus mollis. Vivamus rhoncus augue ac pulvinar rutrum. Aenean vehicula, lorem nec tempus auctor, nunc massa porta erat, quis commodo ipsum ante sed tortor.
                <div class="rq"></div>
                <div class="lq"></div>
            </div>
        </div>
    </div>
</div>
<script>
    function positon_location(index) {
        switch (index) {
            case 0:
                $('.show_room').hide();
                $('.vegetable_section').show();
                return -510;
                break;
            case 1:
                $('.show_room').hide();
                $('.flower_section').show();
                return -557;
                break;
            case 2:
                $('.show_room').hide();
                $('.fruit_section').show();
                return -606;
                break;
            case 3:
                $('.show_room').hide();
                $('.all_section').show();
                return -658;
                break;
            default:
                alert('Data Mismatch');
                break;
        }
    }
    $(function() {
        var index = 2;
        $('a.mode_prev').click(function() {
            if ((index - 1) >= 0) {
                index--;
                var loc = positon_location(index);
                $('.display_switch').animate({backgroundPositionX: loc}, 300);
            } else {
                $('.display_switch').animate({backgroundPositionX: -505}, 200, function() {
                    $('.display_switch').animate({backgroundPositionX: -510}, 200);
                });
            }
        });

        $('a.mode_next').click(function() {
            if ((index + 1) < 4) {
                index++;
                var loc = positon_location(index);
                $('.display_switch').animate({backgroundPositionX: loc}, 300);
            } else {
                $('.display_switch').animate({backgroundPositionX: -663}, 200, function() {
                    $('.display_switch').animate({backgroundPositionX: -658}, 200);
                });
            }
        });

        $('a.mode_next').trigger('click');

        $('.shell_right, .shell_left').mouseenter(function() {
            $(this).stop().animate({opacity: 0}, 400);
        }).mouseleave(function() {
            $(this).stop().animate({opacity: 1}, 400);
        });
    });
</script>