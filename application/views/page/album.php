<div class="gallery_garden_header section_one_parallax parallax_section">
    <h1 class="white txt_center diavlo" style="font-size: 85px; padding: 80px 0px; margin: 0px auto;">Gallery</h1>
</div>
<div class="container section_two_parallax">
    <h3 class="diavlo white trycatch_nav">
        <a href="<?php echo site_url(($lang == "th") ? "page/th/index" : ""); ?>" class="a-no-action">Home</a> <span>&raquo;</span>
        <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "") . "gardening"); ?>" class="a-no-action">Gallery</a> <span>&raquo;</span>
        <span><?php echo ($lang == "th") ? $name->th : $name->en; ?></span>
    </h3>
    <div class="row-fluid">
        <?php foreach ($img_list as $key => $rec): ?>
            <a href="<?php echo $rec->img_url; ?>" rel="gallery" class="span3 garden-each-gall colorbox a-no-action <?php echo ($key % 4) ? "" : "alpha"; ?>" style="margin-top: 20px;">
                <div class="thumbnail thumbnail-garden" style="background-image: url('<?php echo $rec->img_url; ?>'); position: relative;">
                    <div class="magnifier"></div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
    <div class="row-fluid" style="margin-top: 20px;">
        <div class="span12">
            <h4 class="white diavlo"><?php echo ($lang == "th") ? $name->th : $name->en; ?></h4>
            <p><?php echo ($lang == "th") ? $detail->th : $detail->en; ?></p>
        </div>
    </div>
</div>