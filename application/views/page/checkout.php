<div class="container">
    <div class="checkout_panel" style="padding: 30px 0px;">
        <div id="first_action">
            <div class="onion_rotate">
                <a name="onion"></a>
            </div>
            <h3 class="txt_center white diavlo process_title">Processing your request ...</h3>
            <p class="please_wait">Please wait</p>
        </div>
        <div id="second_action">
            <div class="done_png" style="float:left; margin-right: 10px;"></div>
            <div style="float: left;">
                <h1 style="font-size: 72px;" class="diavlo white">DONE!</h1>
                <h3 style="font-size: 24px; text-indent: 5px; margin: 2px auto;" class="white diavlo_light">Your request has been sent to us, Thank you</h3>
                <p style="font-size: 12px; text-indent: 5px;" class="white margin-bottom-none">Your order has been sent to our system. We will</p>
                <p style="font-size: 12px; text-indent: 5px;" class="white">contact you back as soon as possible.</p>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>
</div>
<style>
    .process_title{
        margin: auto;
        position: absolute;
        top: 225px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        display: block;
        height: 40px;
    }

    .please_wait{
        margin: auto;
        position: absolute;
        top: 285px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        display: block;
        height: 20px;
        color: #fff;
        text-align: center;
    }
    #second_action{
        position: absolute;
        top: 0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        margin: auto;
        height: 140px;
        width: 690px;
        opacity: 0;
    }
</style>
<script type="text/javascript">
    $(function() {
        var angle = 0;
        var onion = setInterval(function() {
            angle += 10;
            $('.onion_rotate').rotate(angle);
        }, 50);

        setTimeout(function() {
            $('#first_action').animate({opacity: 0}, 1000, function() {
                clearInterval(onion);
                $('#first_action').remove();
                $('#second_action').animate({opacity: 1}, 200);
                setTimeout(function() {
                    location.href = "<?php echo site_url(); ?>";
                }, 2000);
            });
        }, 5000);

        $.get("<?php echo site_url('page/email_order/' . $transaction_id); ?>", {test:'test'}, function(res) {
            console.log(res);
        });
    });
</script>