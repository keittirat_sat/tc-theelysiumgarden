<div class="container" style="padding-top: 50px;">
    <!--Company History-->
    <div class="row-fluid" style="margin-bottom: 30px;">
        <div class="span7">
            <?php echo image_asset("gardening.png"); ?>
        </div>
        <div class="span5 white txt_left">
            <h2 class="diavlo">Gardening</h2>
            <p>ห้างหุ้นส่วนจำกัด ดิ อีลิซเซียม การ์เด้น เดิมจดทะเบียนชื่อร้านมาจิคสเคป เมื่อวันที่ 14 กรกฎาคม 2553 ดำเนินกิจการเกี่ยวกับรับออกแบบพร้อมจัดสวนภูมิทัศน์ จำหน่ายพันธุ์ไม้และวางระบบน้ำสวนอัตโนมัติ ที่จังหวัดเชียงใหม่ และเมื่อวันที่ 26 มิถุนายน  2556 ได้จดทะเบียนจัดตั้งเป็น ห้างหุ้นส่วนจำกัด ดิ อีลิซเซียม การ์เด้น เพื่อให้สอดคล้องกับการขยายขอบเขตงาน ในส่วนองค์กรราชการรัฐวิสาหกิจและเอกชนขนาดใหญ่ เพื่อให้มั่นใจในขีดความสามารถในการดำเนินงานและบริการโดยห้างหุ้นส่วนจำกัด ยังคงดำเนินงานเกี่ยวกับการจัดสวนภูมิทัศน์ ดูแลสวน วางระบบน้ำอัตโนมัติ และได้เพิ่มส่วนงานจำหน่าย ผลิตผลทางการเกษตร พืชผัก, ผลไม้ ปลอดสารพิษ ตลอดจนจำหน่ายดอกไม้สดเมืองหนาว ให้กับลูกค้าในเขตพื้นที่ภาคใต้ โดยจะเริ่มให้บริการและจัดจำหน่ายได้ ตั้งแต่เดือนตุลาคม 2556 เป็นต้นไป ที่จังหวัดกระบี่, พังงา, ตรัง, ภูเก็ต และจังหวัดใกล้เคียง </p>
        </div>
    </div>
    <!--End Company history-->

    <!--List of Job-->
    <div class="row-fluid white job_dec">
        <div class="span6">
            <div class="garden-icon1"></div>
            <h3 class="txt_center">การจัดสวน</h3>
            <p class="txt_right"><?php echo image_asset("green_dot.png"); ?>ออกแบบงานจัดสวนภูมิทัศน์ หลากหลายสไตล์</p>
            <p class="txt_right"><?php echo image_asset("green_dot.png"); ?>จัดสวนภูมิทัศน์ตามแบบของลูกค้า, ปลูกต้นไม้ใหญ่, ไม้พุ่ม, ไม้คลุมดิน, หญ้าสนามทุกชนิด</p>
            <p class="txt_right"><?php echo image_asset("green_dot.png"); ?>ดูแลรักษาสวน, ใส่ปุ๋ย, ตัดแต่งต้นไม้, ตัดหญ้าสนาม</p>
            <p class="txt_right"><?php echo image_asset("green_dot.png"); ?>รับจัดหา, จำหน่าย พันธุ์ไม้เพื่องานภูมิทัศน์</p>
            <p class="txt_right"><?php echo image_asset("green_dot.png"); ?>จำหน่าย หญ้าสนาม, หญ้านวลน้อย, หญ้ามาเลเซีย, หญ้าเบอร์มิวด้า,<br/>หญ้าญี่ปุ่น และหญ้าเทียม</p>
        </div>
        <div class="span6 garden-vertical-seperate" style="margin-left:0px; padding-left:10px;" >
            <div class="garden-icon2"></div>
            <h3 class="txt_center">ให้บริการ</h3>
            <p class="txt_left"><?php echo image_asset("yellow_dot.png"); ?>กลุ่มโรงแรมรีสอร์ท, ภัตตาคารร้านอาหาร, สนามกอล์ฟ</p>
            <p class="txt_left"><?php echo image_asset("yellow_dot.png"); ?>กลุ่มหน่วยงานราชการรัฐวิสาหกิจ ทุกหน่วยงาน</p>
            <p class="txt_left"><?php echo image_asset("yellow_dot.png"); ?>กลุ่มประกอบการกิจการอสังหาริมทรัพย์ บ้านจัดสรร, คอนโด, รับเหมาก่อสร้าง</p>
            <p class="txt_left"><?php echo image_asset("yellow_dot.png"); ?>กลุ่มผู้ประกอบการให้เช่าสนามฟุตบอล</p>
            <p class="txt_left"><?php echo image_asset("yellow_dot.png"); ?>สำนักงานธุรกิจ, บ้านพัก</p>
        </div>
    </div>
    <!--End pst of job-->
</div>
<div class="gallery_garden_header parallax_section">
    <h1 class="white txt_center diavlo" style="font-size: 85px; padding: 80px 0px; margin: 0px auto;">Gallery</h1>
</div>
<div class="container" style="padding-top: 10px;">
    <div class="row-fluid">
        <?php foreach ($gallery as $key => $rec): ?>
            <?php $name = json_decode($rec->pname); ?>
            <a href="<?php echo site_url("page/" . (($lang == "th") ? "th/" : "en/") . "album/{$rec->pid}"); ?>" class="span3 garden-each-gall a-no-action <?php echo ($key % 4) ? "" : "alpha"; ?>" style="margin-top: 20px;">
                <div class="thumbnail thumbnail-garden" style="background-image: url('<?php echo $rec->img_url; ?>'); position: relative;">
                    <div class="magnifier"></div>
                </div>
                <h4 class="txt_center white"><?php echo ($lang == 'en') ? $name->en : $name->th; ?></h4>
            </a>
        <?php endforeach; ?>
    </div>
</div>