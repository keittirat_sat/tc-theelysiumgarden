<div class="gallery_garden_header section_one_parallax parallax_section">
    <!--heigh 200px-->
    <h1 class="white txt_center diavlo" style="font-size: 85px; padding: 80px 0px; margin: 0px auto;">Contact Us</h1>
</div>
<div class="container section_two_parallax">
    <div class="row-fluid">
        <div class="span6 txt_right info-contact">
            <h2 class="diavlo white">The Elysium Garden<br/>Limited Partnership</h2>
            <?php if ($lang == "th"): ?>
                <!--p class="infotxt">98/3 ม.2 ต.คลองท่อมใต้ อ.คลองท่อม จ.กระบี่ 81120</p-->
                <p class="infotxt">เลขที่ 153 หมู่ 1 ต.สันทรายหลวง อ.สันทราย จ.เชียงใหม่ 50210</p>
                <p class="infotxt"><b>โทร</b> 084-3671262, 083-3228008 , 075-640044</p>
                <p class="infotxt"><b>โทรสาร</b> 075-640044</p>
            <?php else: ?>
                <!--p class="infotxt">98/3 Moo.2 T.Khlong Thom Tai A.Khlong Thom Krabi Thailand 81120</p-->
                <p class="infotxt">153 Moo.1 T.Samsailuang A.Samsai Chiangmai Thailang 50210</p>
                <p class="infotxt"><b>Tel.</b> 084-3671262, 083-3228008 , 075-640044</p>
                <p class="infotxt"><b>Fax.</b> 075-640044</p>
            <?php endif; ?>
            <h2 class="white diavlo" style="margin-top: 15px;">Message to us</h2>
            <form action="http://www.google.com" method="post" class="form-horizontal" id='contact_form'>
                <div class="row-fluid" style="margin-bottom: 15px;">
                    <div class="input-prepend" style="width:100%;">
                        <span class="add-on"><i class="icon icon-user"></i></span>
                        <input class="span8" type="text" placeholder="Name" name="contact-name" required>
                    </div>
                </div>
                <div class="row-fluid" style="margin-bottom: 15px;">
                    <div class="input-prepend" style="width:100%;">
                        <span class="add-on"><i class="icon icon-envelope"></i></span>
                        <input class="span8" type="email" placeholder="Email" name="contact-email" required>
                    </div>
                </div>
                <p><textarea style="width: 325px; resize: none; height: 185px;" name="contact-detail"></textarea></p>
                <p><button class="btn" id='contact_btn' data-loading-text="Sending...">SEND</button></p>
            </form>
        </div>
        <div class="span6">
            <div id="map"></div>
        </div>
    </div>
</div>
<!-- fix the effect of GG-map from bootstrap -->
<style>
    #map img {
        max-width: none;
    }
</style>
<script type="text/javascript">
    $(function() {
        $('#contact_form').submit(function() {
            $('#contact_btn').button('loading');
            var name = $('[name=contact-name]').val();
            var email = $('[name=contact-email]').val();
            var msg = $('[name=contact-detail]').val();
            var f = true;

            if (!name) {
                f = false;
            }

            if (!email) {
                f = false;
            }

            if (!msg) {
                f = false;
            }

            if (f) {
                $.post('<?php echo site_url('api/contact_us'); ?>', {'contact-name': name, 'contact-email': email, 'contact-detail': msg}, function(res) {
                    console.log(res);
                    alert('We have been recieved your email, we will contact you back soon');
                    $('#contact_btn').button('reset');
                    location.reload();
                }, 'json');
            } else {
                alert('กรุณากรอกข้อมูลให้ครบทุกช่อง');
                $('#contact_btn').button('reset');
            }

            return false;
        });
        $('#map').gmap({
            'mapTypeId': google.maps.ROADMAP,
            'zoom': 15,
            'center': '7.941198,99.142685'
        }).bind('init', function(ev, map) {
            $('#map').gmap('addMarker', {'position': '7.941198,99.142685'}).click(function() {
                var marker = "<h3 class='diavlo'>The Eleysium Garden</h3>";
                $('#map').gmap('openInfoWindow', {'content': marker}, this);
            });
        });
    });
</script>