<h3>ใบรายงานการสั่งซื้อสินค้า | The Elysium Garden</h3>
<p>New Order - detail</p>
<?php $vat = $this->getdata->get_vat(); ?>
<table class="table tableborder">
    <tbody>
        <tr>
            <td><b>รหัสลูกค้า :</b></td>
            <td><?php echo "LAN-" . str_pad($order->uid, 5, "0", STR_PAD_LEFT); ?></td>
        </tr>
        <tr>
            <td><b>ชื่อลูกค้า :</b></td>
            <td><?php echo $order->name; ?></td>
        </tr>
        <tr>
            <td><b>ที่อยู่ :</b></td>
            <td><?php echo $order->address1 . ((!empty($order->address2)) ? "<br/>" . $order->address2 : "") . "<br/>" . $order->district . "<br/>" . $order->province . " " . $order->zip; ?></td>
        </tr>
        <tr>
            <td><b>โทรศัพท์ :</b></td>
            <td><?php echo $order_user[0]->telephone; ?></td>
        </tr>
        <tr>
            <td><b>Email :</b></td>
            <td><?php echo $order_user[0]->email; ?></td>
        </tr>
        <tr>
            <td><b>เลขที่ :</b></td>
            <td>
                <?php echo $this->convert_id->order_id($order->order_id, $order->order_date); ?>
            </td>
        </tr>
        <tr>
            <td><b>วันที่สั่งซื้อ :</b></td>
            <td><?php echo date('d-m-Y', $order->order_date); ?></td>
        </tr>
        <tr>
            <td><b>เครดิต :</b></td>
            <td>15 วัน</td>
        </tr>
        <tr>
            <td><b>กำหนดจ่าย :</b></td>
            <td><?php echo date('d-m-Y', $order->credit_date); ?></td>
        </tr>
    </tbody>
</table>
<table class="table table-hover">
    <thead>
        <tr>
            <th class="txt_center">ลำดับ</th>
            <th class="txt_center">รหัสสินค้า</th>
            <th class="txt_center">รายการ</th>
            <th class="txt_center">ราคา/หน่วย</th>
            <th class="txt_center">จำนวน</th>
            <th class="txt_center">จำนวนเงิน</th>
        </tr>
    </thead>
    <tbody>
        <?php $all = 0; ?>
        <?php foreach ($order_item as $key => $item): ?>
            <tr>
                <td class="txt_center"><?php echo $key + 1; ?></td>
                <td class="txt_center">
                    <?php switch ($item->product_detail->category): case 'fruit': ?>
                            <?php $mode = "Po"; ?>
                            <?php break; ?>
                        <?php case 'vagetable': ?>
                            <?php $mode = "Ve"; ?>
                            <?php break; ?>
                        <?php case 'flower': ?>
                            <?php $mode = "Fo"; ?>
                            <?php break; ?>
                    <?php endswitch; ?>
                    <?php echo "{$mode}-{$item->pid}"; ?>
                </td>
                <td>
                    <?php $name = json_decode($item->product_detail->pname) ?>
                    <?php echo $name->th; ?>
                </td>
                <td class="txt_right">
                    ฿<?php echo $item->product_detail->price; ?>
                </td>
                <td class="txt_right">
                    <?php echo $item->amount; ?>
                </td>
                <td class="txt_right">
                    <?php $price = $item->amount * $item->product_detail->price; ?>
                    ฿<?php echo $price; ?>
                    <?php $all += $price; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="5" class="txt_right"><b>ภาษีมูลค่าเพิ่ม <?php echo "{$vat}%"; ?></b></td>
            <td class="txt_right"><?php echo "฿" . $all * ($vat / 100); ?></td>
        </tr>
        <tr>
            <td colspan="5" class="txt_right"><b>ราคารวม</b></td>
            <td class="txt_right"><?php echo "฿" . $all + ($all * ($vat / 100)); ?></td>
        </tr>
    </tbody>
</table>