<?php $total = 0; ?>
<?php foreach ($cart as $index => $product): ?>
    <?php switch ($product['category']): case 'fruit': ?>
            <?php $color = "rgba(255,204,0,0.25)"; ?>
            <?php break; ?>
        <?php case 'vagetable': ?>
            <?php $color = "rgba(153,255,51,0.25)"; ?>
            <?php break; ?>
        <?php case 'flower': ?>
            <?php $color = "rgba(255,102,204,0.25)"; ?>
            <?php break; ?>
    <?php endswitch; ?>

    <tr style="color: #222; font-size: 15px; background-color: <?php echo $color; ?>;">
        <td><?php echo $product['category']; ?></td>
        <td style="text-align: left; "><?php echo ($lang == 'en') ? $product['name']->en : $product['name']->th; ?></td>
        <td style="text-align: right;"><?php echo $product['priece']; ?></td>
        <td style="text-align: right;"><?php echo $product['price']; ?></td>
        <td style="text-align: right;"><?php echo $product['total']; ?> THB.</td>
    </tr>
    <?php $total += $product['total']; ?>
<?php endforeach; ?>
<tr>
    <td colspan="4" style="text-align: right; font-size: 15px; color: #99cc66;"><b>Total price</b></td>
    <td style="text-align: right;"><?php echo "{$total}" ?><span style="font-size: 15px; line-height: 20px;"> THB.</span></td>
</tr>