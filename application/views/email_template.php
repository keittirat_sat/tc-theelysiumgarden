<div style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 20px; color: #222; margin: 0px; padding: 0px;">
    <table style="width: 100%;">
        <tbody>
            <tr style="background-image: linear-gradient(rgba(135, 195, 35, 1), rgba(255, 255, 255, 0) 100%);">
                <th colspan="2" style="margin:0px; padding: 15px 10px; font-size: 22px;">
                    <b>
                        ใบรายงานการสั่งซื้อสินค้า | The Elysium Garden
                    </b>
                </th>
            </tr>

            <tr style="background-image: linear-gradient(rgba(219, 219, 219, 1), rgba(255, 255, 255, 0) 100%); ">
                <th colspan="2" style="margin:0px; padding: 15px 10px; font-size: 18px;">
                    <b>รายละเอียดใบสั่งซื้อ</b>
                </th>
            </tr>
            <tr>
                <th style="width: 150px; text-align: right;">
                    เลขที่ใบสั่งซื้อ:&nbsp;
                </th>
                <td>
                    <?php echo $this->convert_id->order_id($order->order_id, $order->order_date); ?>
                </td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    วันที่สั่งซื้อ:&nbsp;
                </th>
                <td><?php echo date('d F Y', $order->order_date); ?></td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    เครดิต:&nbsp;
                </th>
                <td>
                    <?php echo ($order->transport_date > 0) ? $this->convert_id->cal_date_diff($order->transport_date, $order->credit_date) . " วัน" : "รอการติดต่อจากเรา"; ?>
                </td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    กำหนดจ่าย:&nbsp;
                </th>
                <td><?php echo ($order->credit_date > 0) ? date('d F Y', $order->credit_date) : "รอการตอบกลับจากทางเรา"; ?></td>
            </tr>
            <tr style="background-image: linear-gradient(rgba(219, 219, 219, 1), rgba(255, 255, 255, 0) 100%); ">
                <th colspan="2" style="margin:0px; padding: 15px 10px; font-size: 18px;">
                    <b>รายละเอียดในการส่งสินค้า</b>
                </th>
            </tr>
            <tr>
                <th style="text-align: right;">
                    ชื่อลูกค้า/บริษัท:&nbsp;
                </th>
                <td><?php echo $order->name; ?></td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    อีเมลล์:&nbsp;
                </th>
                <td><?php echo $order_user[0]->email; ?></td>
            </tr>
            <tr>
                <th style="text-align: right; vertical-align: top;">
                    ที่อยู่ในการจัดส่ง:&nbsp;
                </th>
                <td><?php echo $order->address1 . ((!empty($order->address2)) ? "<br/>" . $order->address2 : "") . "<br/>" . $order->district . "<br/>" . $order->province . " " . $order->zip; ?></td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    โทรศัพท์:&nbsp;
                </th>
                <td><?php echo $order_user[0]->telephone; ?></td>
            </tr>
            <tr>
                <th style="text-align: right;">
                    โทรสาร:&nbsp;
                </th>
                <td><?php echo $order_user[0]->fax; ?></td>
            </tr>

            <?php if ($can_edit): ?>
                <tr>
                    <th style="text-align: right;">
                        กำหนดชำระ:&nbsp;
                    </th>
                    <td>
                        <!--It's can edit-->
                        <div class="input-append">
                            <input type='text' name='due_date' id='due_date' readonly style='cursor: pointer;' class="{order_id:'<?php echo $order->order_id; ?>'}" <?php echo ($order->credit_date > 0) ? "value='" . date('m/d/Y', $order->credit_date) . "'" : ""; ?>>
                            <span class="add-on"><i class='icon icon-calendar'></i></span>
                        </div>
                    </td>
                </tr>
            <?php endif; ?>

            <tr>
                <th style="text-align: right;">
                    สถานะของใบสั่งซื้อ:&nbsp;
                </th>
                <td>
                    <?php if ($can_edit): ?>
                        <div class="input-append">
                            <select class="state_dropdown {order_id:'<?php echo $order->order_id; ?>'}">
                                <?php if ($order->status < 2): ?>
                                    <option value="0" <?php echo ($order->status == 0) ? "selected" : ""; ?>>ใบสั่งซื้อเข้ามาใหม่</option>
                                    <option value="1" <?php echo ($order->status == 1) ? "selected" : ""; ?>>กำลังดำเนินการ</option>
                                <?php endif; ?>
                                <option value="2" <?php echo ($order->status == 2) ? "selected" : ""; ?>>ส่งแล้วเรียบร้อยแล้ว</option>
                                <option value="3" <?php echo ($order->status == 3) ? "selected" : ""; ?>>ชำระเงินเรียบร้อยแล้ว</option>
                            </select>
                            <button class="btn" type="button" id="update_status"><i class='icon icon-refresh'></i> UPDATE</button>
                        </div>
                    <?php else: ?>
                        <?php switch ($order->status): case 0: ?>
                                รอการตอบกลับจากทางเรา ...
                                <?php break; ?>
                            <?php case 1: ?>
                                กำลังดำเนินการเตรียมสินค้าในการจัดส่ง ...
                                <?php break; ?>
                            <?php case 2: ?>
                                ส่งเรียบร้อยแล้ว
                                <?php break; ?>
                            <?php case 3: ?>
                                ชำระเงินเรียบร้อยแล้ว
                                <?php break; ?>
                        <?php endswitch; ?>
                    <?php endif; ?>
                </td>
            </tr>
        </tbody>
    </table>

    <table style="width: 100%; border-spacing: 0px; border-collapse: collapse;">
        <thead>
            <tr style="background-image: linear-gradient(rgba(38, 89, 255, 0.5), rgba(255, 255, 255, 1) 100%);">
                <th colspan="6" style="margin:0px; padding: 7px 10px; font-size: 18px; border:1px solid #666; ">
                    <b>รายละเอียดการสินค้าที่สั่งซื้อ</b>
                </th>
            </tr>
            <tr style="background-image: linear-gradient(rgba(255, 255, 255, 1), rgba(38, 89, 255, 0.5) 100%); font-size: 16px; text-align: center;">
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    ลำดับ
                </td>
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    รหัสสินค้า
                </td>
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    รายการ
                </td>
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    ราคา/หน่วย
                </td>
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    จำนวน
                </td>
                <td style="margin:0px; padding: 7px 10px; border:1px solid #666; ">
                    จำนวนเงิน
                </td>
            </tr>
        </thead>
        <tbody>
            <?php $all = 0; ?>
            <?php foreach ($order_item as $key => $item): ?>
                <tr>
                    <td style="text-align: center; border:1px solid #666; "><?php echo $key + 1; ?></td>
                    <td style="text-align: center; border:1px solid #666; ">
                        <?php echo $this->convert_id->product_id($item->pid, $item->product_detail->category); ?>
                    </td>
                    <td style=" border:1px solid #666; ">
                        <?php $name = json_decode($item->product_detail->pname) ?>
                        <?php echo $name->th; ?>
                    </td>
                    <td style="text-align: right; border:1px solid #666; ">
                        ฿<?php echo $item->product_detail->price; ?>
                    </td>
                    <td style="text-align: right; border:1px solid #666; ">
                        <?php echo $item->amount; ?>
                    </td>
                    <td style="text-align: right; border:1px solid #666; ">
                        <?php $price = $item->amount * $item->product_detail->price; ?>
                        ฿<?php echo $price; ?>
                        <?php $all += $price; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="5" style="text-align: right; border:1px solid #666; "><b>ราคารวม</b></td>
                <td style="text-align: right; border:1px solid #666; "><?php echo "฿{$all}"; ?></td>
            </tr>
        </tbody>
    </table>
</div>

<?php if ($can_edit): ?>
    <div class="well well-small" style="margin-top: 15px;">
        <h5>ประวัติของเอกสาร</h5>
        <?php $map = directory_map("./pdf/" . $this->convert_id->order_id($order->order_id, $order->order_date, true)); ?>
        <?php if (!empty($map)): ?>
            <ul>
                <?php foreach ($map as $billing): ?>
                    <li><a href="<?php echo site_url("pdf/" . $this->convert_id->order_id($order->order_id, $order->order_date, true) . "/{$billing}"); ?>"><?php echo $billing; ?></a></li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
            -- ไม่พบประวัตเอกสาร --
        <?php endif; ?>
    </div>
    <div class="txt_center" style="margin-top: 15px; padding-bottom: 30px;">
        <button class="btn btn-success {order_id:'<?php echo $order->order_id; ?>'}" id="email_update"><i class="icon icon-envelope icon-white"></i> EMAIL UPDATE</button>
    </div>
    <script type="text/javascript">
        function trigger_update(ref) {
            $.post('<?php echo site_url('api/update_order_state'); ?>', {'order_id': $(ref).metadata().order_id, 'status': $(ref).val()}, function(res) {
                console.log(res);
                var response = $.parseJSON(res);
                if (response.status === 'success') {
                    alert('อัพเดตสถานะแล้ว');
                    location.reload();
                } else {
                    alert('เกิดข้อผิดพลาด');
                }
            });
        }


        $(function() {

            $('#email_update').click(function() {
                var order_id = $(this).metadata().order_id;
                $.get('<?php echo site_url('api/email_order_update'); ?>', {order_id: order_id}, function(res) {
                    if (res.status === "success") {
                        alert("ส่งแล้ว");
                        location.reload();
                    } else {
                        alert("Sever timeout");
                    }
                }, 'json');
            });

    //            $('.state_dropdown').change(function() {
    //                pay_day_dialog();
    //            });

            $('#update_status').click(function() {
                var element = $('.state_dropdown');
                if (($('#transport_date').val() == '0') && ($(element).val() > 1)) {
                    if (confirm("ต้องการเปลี่ยนแปลงสถานะเป็น \"ส่งแล้ว\" ของใบสั่งซื้อเลขที่ " + $('#this_order_id').val() + "หรือไม่ ถ้าเปลี่ยนแปลงแล้วจะไม่สามารถเปลี่ยนแปลงได้อีก")) {
                        trigger_update(element);
                    }
                } else {
                    trigger_update(element);
                }
            });

            var startDate = new Date(<?php echo $order->order_date; ?>);
            var currentDate = 0;

            $('#due_date').datepicker().on('changeDate', function(ev) {
                if (startDate.valueOf() <= ev.date.valueOf()) {
                    if (currentDate !== ev.date.valueOf()) {
                        currentDate = ev.date.valueOf();
                        if (confirm("ยืนยันการกำหนดวันชำระเงิน " + $('#due_date').val())) {
                            var milisecond_date = ev.date.valueOf();
                            var unix_date = milisecond_date / 1000;
                            var order_id = $(this).metadata().order_id;

                            $.post('<?php echo site_url('api/set_pay_date'); ?>', {order_id: order_id, credit_date: unix_date}, function(res) {
                                if (res.status === "success") {
                                    alert("อัพเดตวันชำระเงินสำเร็จ");
                                    location.reload();
                                } else {
                                    alert("ล้มเหลว");
                                }
                            }, 'json');
                        }
                    } else {
                        console.log('Duplicate : ' + currentDate);
                    }
                } else {
                    alert("กำหนดวันจ่ายเงินต้องเป็นวันในอนาคตเท่านั้น");
                    console.log('Do you have a Time machine ? : ' + currentDate);
                }
            });
        });
    </script>
<?php endif; ?>