<div class="span9">
    <legend><h3>Send SMS | <span style="color: green;">สามารถส่งได้อีก <?php echo $sms_credit[0]->cvalue; ?> ฉบับ</span></h3></legend>
    <div class="row-fluid">
        <div class="span8">
            <h4>เงื่อนไขในการใช้บริการ SMS Service จาก TryCatch&trade;</h4>
            <ol>
                <li>ระบบในการให้บริการเป็นแบบ <font style="color: #ff0000;">Pre-paid (เติมเงินก่อนใช้งาน)</font></li>
                <li>ค่าบริการในการส่งต่อ <font style="color: #ff0000;">1 ข้อความ 2.50 บาท</font></li>
                <li>การเติมเงินให้ติดต่อกับทาง TryCatch&trade; โดยโอนเงินผ่านทางธนาคาร</li>
            </ol>
            <textarea class="span12 txt_sms" style="resize: none; height: 250px;"></textarea>
            <p id="sms_counter" style="text-align: right;">จำนวนอักขระ: 0</p>
        </div>
        <div class="span4">
            <div class="well well-small">
                <h4>เบอร์โทรศัพท์</h4>
                <?php foreach ($custom as $key => $value): ?>
                    <label class="checkbox">
                        <input type="checkbox" value="<?php echo $value->cvalue; ?>" checked> <?php echo $value->cvalue; ?>
                    </label>
                <?php endforeach; ?>
                <button class="btn btn-danger {credit:<?php echo $sms_credit[0]->cvalue; ?>}" id="ok_send" style="width:190px;">SEND SMS</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#ok_send').click(function() {
            var sms = $('.txt_sms').val();
            var counter = sms.length;
            var sms_counter = Math.ceil(counter / 70);
            var tel_selected = 0;
            var limit = $(this).metadata().credit;
            var tel = [];
            $('[type=checkbox]').each(function(id, ele) {
                if ($(ele).attr('checked')) {
                    tel_selected++;
                    tel.push($(ele).val());
                }
            });
            var total_sms = sms_counter * tel_selected;
            if (confirm("คุณต้องการส่ง SMS จำนวน " + total_sms + " ฉบับหรือไม่ (Credit: " + limit + ")")) {
                if (total_sms <= limit) {
                    $.post('<?php echo site_url('api/trycatch_sms') ?>', {tel: tel, msg: sms, total_sms: total_sms}, function(res) {
                        console.log(res);
                        if (res.status === "success") {
                            location.reload();
                        }
                    }, 'json');
                } else {
                    alert('Credit ไม่พอในการส่ง');
                }
            }
        });
        $('.txt_sms').keyup(function() {
            var txt = $(this).val();
            var counter = txt.length;
            var txt_counter = "จำนวนอักขระ: " + counter + " | " + Math.ceil(counter / 70) + " ฉบับ";
            $('#sms_counter').text(txt_counter);
        });
    });
</script>