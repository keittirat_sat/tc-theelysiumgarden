<div class="span9">
    <legend>
        <h3>Add new gallery</h3>
    </legend>
    <div class="row-fluid">
        <div class="span8">
            <div class="accordion" id="accordion">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            English
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <div class="accordion-inner">
                            <div class="row-fluid">
                                <input type="text" id="title_en" for="product_name_en" name="title_en" class="span12" placeholder="Product's name">
                                <textarea class='span12' name='msg_en' placeholder="Product's detail" for="product_detail_en"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            ภาษาไทย
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <div class="row-fluid">
                                <input type="text" id="title_th" for="product_name_th" name="title_th" class="span12" placeholder="ชื่อผลิตภัณฑ์">
                                <textarea class='span12' name='msg_th' placeholder='รายละเอียดสินค้า' for="product_detail_th"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <form action='<?php echo site_url('api/upload_post'); ?>' method='post' id="form_product_upload">
                <div class="well">
                    <div class="feature-image"></div>
                    <button class="btn btn-info upload_btn" id='modal_open' type='button' style="width: 190px; position: relative; margin-bottom: 10px;">ADD IMAGES</button>
                    <button class="btn btn-primary" style="width: 190px;">PUBLISH</button>
                </div>
                <input type="hidden" name="quantity" value="0">
                <input type='hidden' name='product_name_en'>
                <input type='hidden' name='product_name_th'>
                <input type='hidden' name='product_detail_th'>
                <input type='hidden' name='product_detail_en'>
                <input type='hidden' name='product_image'>
                <input type='hidden' name="category" value="gallery">
                <input type='hidden' name='post_type' value="gallery">
                <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
            </form>
        </div>
    </div>
</div>

<div class="modal">
    <div class="modal-body">
        <ul class="nav nav-tabs" style='margin-bottom: 10px;'>
            <li class='active'><a href="#allimage" data-toggle="tab" id="first_tab">Images in this album</a></li>
            <li><a href="#upload" data-toggle="tab">Upload to this album</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="allimage">
                <div class="row-fluid">
                    <div class="alert alert-info">
                        เลือกที่รูปเพื่อตั้งค่ารูปหน้าปกอัลบั้ม
                    </div>
                </div>
                <div class='row-fluid uploaded_img'>

                </div>
            </div>
            <div class="tab-pane" id="upload">
                <form action='<?php echo site_url('api/upload_album_image'); ?>' method='post' id='image_form' enctype="multipart/form-data" style='position: relative;'>
                    <button class="btn upload_btn upload_multi">
                        SELECT FILES
                        <input type='file' name='myimage[]' id='file_upload' multiple>
                    </button>
                    <div style="padding-top: 130px;">
                        <div class="progress progress-striped active" style="margin: 0px;">
                            <div class="bar" style="width: 40%;"></div>
                        </div>
                    </div>
                    <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" id='close_modal'>Close</a>
    </div>
</div>

<style>
    textarea{
        resize: none;
        height: 350px;
    }

    .accordion-heading{
        background-color: #efefef;
        text-shadow: 1px 1px 3px #fff;
    }

    #file_upload{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        z-index: 99;
        opacity: 0;
    }

    .upload_multi{
        width: 190px;
        height: 50px;
        position: absolute;
        margin: auto;
        top: 0px;
        bottom: 0px;
        left: 0px;
        right: 0px;
    }
    .modal{
        width: 600px;
        height: 400px;
        position: absolute;
        top: 0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        margin: auto;
    }

    .modal .modal-body{
        height: 340px;
        padding: 0px;
    }

    .modal form#image_form{
        height: 280px;
        margin: 0px;
    }

    .modal .tab-content{
        height: 280px;
        margin: 0px 10px 10px;
    }

    #allimage .span3{
        height: 140px;
        background-repeat: no-repeat;
        background-position: center;
        background-size: auto 100%;
    }
</style>
<script>
    function getextension(filename) {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
    }

    function getReadableFileSizeString(fileSizeInBytes) {
        var i = -1;
        var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    }

    $(function() {
        $('.modal').hide();
        $('.progress').hide();

        $('#myTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $('#modal_open').click(function() {
            $('.modal').show();
        });

        $('#close_modal').click(function() {
            $('.modal').hide();
        });

        $('#file_upload').change(function() {
            var limit = this.files.length;
            $(this.files).each(function(id, ele) {
                var ext = getextension(ele.name);
                var size = getReadableFileSizeString(ele.size);
                switch (ext[0].toLowerCase()) {
                    case 'jpg':
                    case 'png':
                    case 'jpeg':
                        console.log(id, ele.name, ext[0].toLowerCase(), size);
                        break;
                    default:
                        alert('ไม่อนุญาตให้อัพโหลด file นามสกุล *.' + ext[0].toLowerCase());
                        $('form#image_form').trigger('reset');
                        return;
                        break;
                }
            });

            if (limit > 0) {
                $('form#image_form').submit();
            }
        });

        $('form#image_form').ajaxForm({
            beforeSend: function()
            {
                $('.progress').show();
                $('.bar').css({width: "0%"});
                $('.upload_multi').hide();
                $('.thumbnail-img').html("").hide();
            },
            uploadProgress: function(event, position, total, percentComplete)
            {
                $('.bar').css({width: percentComplete + "%"});
            },
            complete: function(xhr)
            {
                $('.progress').hide();
                $('.upload_multi').show();
                var json = $.parseJSON(xhr.responseText);
                $(json).each(function(id, ele) {
                    if (id % 4)
                        alpha = '';
                    else
                        alpha = ' alpha';

                    var temp = "<div class='span3 demo-image {img_id:" + ele.img_id + "} " + alpha + "' data-url='" + ele.img_url + "' style='background-image:url(\"" + ele.img_url + "\"); margin-bottom:10px;'></div>";
                    $('.uploaded_img').append(temp);
                });
                $('#first_tab').trigger('click');
            }
        });

        $('.demo-image').live({
            click: function() {
                console.log($(this).metadata().img_id, $(this).attr('data-url'));
                var img = "<img src='" + $(this).attr('data-url') + "' width=100%>";
                $('.feature-image').html(img);
                $('[name=product_image]').val($(this).metadata().img_id);
            }
        });

        $('[type=text]').change(function() {
            var target = $(this).attr('for');
            var data = $(this).val();

            $('[name=' + target + ']').val(data);
            console.log(target, data);
        });

        $('textarea').change(function() {
            var target = $(this).attr('for');
            var data = $(this).val();

            $('[name=' + target + ']').val(data);
            console.log(target, data);
        });

        $('form#form_product_upload').ajaxForm({
            complete: function(xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                location.href = json.url;
            }
        });
    });
</script>