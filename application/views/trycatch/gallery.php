<div class="span9">
    <legend>
        <h3>All gallery</h3>
    </legend>
    <div class="row-fluid">
        <?php foreach ($post as $key => $item): ?>
            <?php $title = json_decode($item->pname); ?>
            <a class="span4 product-show <?php echo ($key % 3) ? "" : "alpha"; ?>" href="<?php echo site_url('trycatch/gallery_edit?post_id=' . $item->pid); ?>">
                <div class="thumb-product" style="background-image: url('<?php echo $item->img_url; ?>');"></div>
                <h4 style="text-align: center;">
                    <?php echo "{$title->en}"; ?>
                </h4>
                <h4 style="text-align: center;">
                    <?php echo "{$title->th}"; ?>
                </h4>
            </a>
        <?php endforeach; ?>
    </div>
</div>