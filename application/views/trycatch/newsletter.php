<div class="span9">
    <legend><h3>Send Newsletter</span></h3></legend>
    <div class="row-fluid" style="margin-bottom: 15px;">
        <div class="span12">
            <div class="controls-row">
                <input type="text" placeholder="Subject" name="subject_box" id="subject_box">
            </div>
            <textarea id="newsletter_txt"></textarea>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="well well-small">
                <h4>Email</h4>
                <?php foreach ($custom as $key => $value): ?>
                    <label class="checkbox">
                        <input type="checkbox" value="<?php echo $value->cvalue; ?>" checked> <?php echo $value->cvalue; ?>
                    </label>
                <?php endforeach; ?>
                <button class="btn btn-danger {credit:<?php echo $sms_credit[0]->cvalue; ?>}" id="ok_send" style="width:190px;">SEND Email</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        tinymce.init({
            selector: "textarea"
        });

        $('#ok_send').click(function() {
            var email = [];
            var email_selected = 0;
            var msg = tinymce.get('newsletter_txt').getContent();
            var subject = $('#subject_box').val();
            $('[type=checkbox]').each(function(id, ele) {
                if ($(ele).attr('checked')) {
                    email_selected++;
                    email.push($(ele).val());
                }
            });
            $.post('<?php echo site_url('api/trycatch_newletter'); ?>', {subject: subject, email: email, msg: msg}, function(res) {
                if(res.status === "success"){
                    alert('ระบบได้ดำเนินตามคำขอสำเร็จ');
                    location.reload();
                }
            },'json');
        });
    });
</script>