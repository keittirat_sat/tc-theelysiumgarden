<div class="row-fluid" style="padding-top: 20px;">
    <div class="alert alert-danger span12">
        <h4>Error</h4>
        ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบผู้ใช้งาน และรหัสเข้าระบบ
    </div>
</div>
<div class="login-form span3">
    <?php echo image_asset("logo_backends.png"); ?>
    <form action="http://www.google.com" method="post" id="login_form_section">
        <label><input type="text" name="username" placeholder="Username"></label>
        <label><input type="password" name="password" placeholder="Password"></label>
        <button class="btn">SUBMIT</button>
    </form>
</div>
<script type="text/javascript">
    $(function() {

        $('.alert').hide();
        $('form#login_form_section').submit(function() {
            $('.alert').hide();
            $('.login-form').css({'box-shadow': '0px 0px 5px #666'});

            var user = $('[name=username]').val();
            var pass = $('[name=password]').val();
            var flag = true;

            if (!user) {
                flag = false;
            }

            if (!pass) {
                flag = false;
            }

            if (flag) {
                $.post("<?php echo site_url("api/login"); ?>", {username: user, password: pass}, function(res) {
                    console.log(res);
                    if (res.status === "success") {
                        location.href = res.url;
                    } else {
                        $('.alert').show();
                        $('.login-form').css({'box-shadow': '0px 0px 5px #ff0000'});
                    }
                }, 'json');
            }

            return false;
        });
    });
</script>