<div class="span9">
    <legend><h3>Order ID : <?php echo $order->order_id; ?></h3></legend>
    <div class="row-fluid" style="position: relative;">
        <h3 class="txt_left">รายละเอียดใบสั่งซื้อ</h3>
        <select class="state_dropdown {order_id:'<?php echo $order->order_id; ?>'}">
            <option value="0" <?php echo ($order->status == 0) ? "selected" : ""; ?>>ใบสั่งซื้อเข้ามาใหม่</option>
            <option value="1" <?php echo ($order->status == 1) ? "selected" : ""; ?>>กำลังดำเนินการ</option>
            <option value="2" <?php echo ($order->status == 2) ? "selected" : ""; ?>>ส่งแล้ว - เครดิต</option>
            <option value="3" <?php echo ($order->status == 3) ? "selected" : ""; ?>>ส่งแล้ว - จ่ายเงินแล้ว</option>
        </select>
    </div>
    <div class="row-fluid">
        <div class="span5">
            <table class="table tableborder">
                <tbody>
                    <tr>
                        <td><b>รหัสลูกค้า :</b></td>
                        <td><?php echo "ELY-" . $order->uid; ?></td>
                    </tr>
                    <tr>
                        <td><b>ชื่อลูกค้า :</b></td>
                        <td><?php echo $order->name; ?></td>
                    </tr>
                    <tr>
                        <td><b>ที่อยู่ :</b></td>
                        <td><?php echo $order->address1 . ((!empty($order->address2)) ? "<br/>" . $order->address2 : "") . "<br/>" . $order->district . "<br/>" . $order->province . " " . $order->zip; ?></td>
                    </tr>
                    <tr>
                        <td><b>โทรศัพท์ :</b></td>
                        <td><?php echo $order_user[0]->telephone; ?></td>
                    </tr>
                    <tr>
                        <td><b>Email :</b></td>
                        <td><?php echo $order_user[0]->email; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="span7">
            <table class="table tableborder">
                <tbody>
                    <tr>
                        <td><b>เลขที่ :</b></td>
                        <td><?php echo $order->order_id; ?></td>
                    </tr>
                    <tr>
                        <td><b>วันที่สั่งซื้อ :</b></td>
                        <td><?php echo date('d-m-Y', $order->order_date); ?></td>
                    </tr>
                    <tr>
                        <td><b>เครดิต :</b></td>
                        <td>15 วัน</td>
                    </tr>
                    <tr>
                        <td><b>กำหนดจ่าย :</b></td>
                        <td><?php echo date('d-m-Y', $order->credit_date); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row-fluid">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th class="txt_center">ลำดับ</th>
                    <th class="txt_center">รหัสสินค้า</th>
                    <th class="txt_center">รายการ</th>
                    <th class="txt_center">ราคา/หน่วย</th>
                    <th class="txt_center">จำนวน</th>
                    <th class="txt_center">จำนวนเงิน</th>
                </tr>
            </thead>
            <tbody>
                <?php $all = 0; ?>
                <?php foreach ($order_item as $key => $item): ?>
                    <tr>
                        <td class="txt_center"><?php echo $key + 1; ?></td>
                        <td class="txt_center">
                            <?php switch ($item->product_detail->category): case 'fruit': ?>
                                    <?php $mode = "Po"; ?>
                                    <?php break; ?>
                                <?php case 'vagetable': ?>
                                    <?php $mode = "Ve"; ?>
                                    <?php break; ?>
                                <?php case 'flower': ?>
                                    <?php $mode = "Fo"; ?>
                                    <?php break; ?>
                            <?php endswitch; ?>
                            <?php echo "{$mode}-{$item->pid}"; ?>
                        </td>
                        <td>
                            <?php $name = json_decode($item->product_detail->pname) ?>
                            <?php echo $name->th; ?>
                        </td>
                        <td class="txt_right">
                            ฿<?php echo $item->product_detail->price; ?>
                        </td>
                        <td class="txt_right">
                            <?php echo $item->amount; ?>
                        </td>
                        <td class="txt_right">
                            <?php $price = $item->amount * $item->product_detail->price; ?>
                            ฿<?php echo $price; ?>
                            <?php $all += $price; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5" class="txt_right"><b>ราคารวม</b></td>
                    <td class="txt_right"><?php echo "฿{$all}"; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<style>
    .state_dropdown{
        position: absolute;
        top: 15px;
        right: 0px;
    }
</style>
<script>
    $(function() {
        $('.state_dropdown').change(function() {
            $.post('<?php echo site_url('api/update_order_state'); ?>', {'order_id': $(this).metadata().order_id, 'status': $(this).val()}, function(res) {
                if (res.status === 'success') {
                    alert('อัพเดตสถานะแล้ว');
                    location.reload();
                } else {
                    alert('เกิดข้อผิดพลาด');
                }
            }, 'json');
        });
    });
</script>