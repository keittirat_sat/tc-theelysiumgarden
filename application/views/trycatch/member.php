<div class='span9'>
    <legend><h3><?php echo $title; ?></h3></legend>
    <div class='row-fluid'>
        <div class='span12'>

            <table class='table table-condensed'>
                <thead>
                    <tr>
                        <th class='txt_center'>#ID</th>
                        <th class='txt_center'>Company / Name</th>
                        <th class='txt_center'>Username</th>
                        <th class='txt_center'>Address</th>
                        <th class='txt_center'>Class</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($userlist as $key => $rec): ?>
                        <tr id="user_<?php echo $rec->uid; ?>" >
                            <td class='txt_center'><?php echo $this->convert_id->user_id($rec->uid); ?></td>
                            <td><?php echo $rec->name; ?></td>
                            <td><?php echo $rec->username; ?></td>
                            <td>
                                <?php echo $rec->address1 . (($rec->address2) ? "<br/>" . $rec->address2 : "") . "<br/>" . $rec->district . "<br/>" . $rec->province . " " . $rec->zip; ?>
                            </td>
                            <td class='txt_center'>
                                <div class="control-group margin-bottom-none">
                                    <div class="controls">
                                        <div class="input-append">
                                            <input class="prefix_code" id="<?php echo "prefix_for_uid_" . $rec->uid; ?>" style="width: 65px;" type="text" placeholder="3 character" maxlength="3" value="<?php echo $rec->prefix_code; ?>">
                                            <button class="btn {uid:<?php echo $rec->uid; ?>} prefix_update_btn" style="margin-left: 0px;" type="button">UPDATE</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group margin-bottom-none">
                                    <div class="controls">
                                        <select style="width: 160px;" for="user_<?php echo $rec->uid; ?>" class="user_level margin-bottom-none {uid:<?php echo $rec->uid; ?>}">
                                            <option <?php echo ($rec->level == 0) ? "selected" : ""; ?> value="0">Admin User</option>
                                            <option <?php echo ($rec->level == 1) ? "selected" : ""; ?> value="1">Normal User</option>
                                            <option <?php echo ($rec->level == 2) ? "selected" : ""; ?> value="2">Ban User</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $('.user_level').change(function() {
            var target = $(this).attr('for');
            var level = $(this).val();
            var uid = $(this).metadata().uid;
            switch (parseInt(level)) {
                case 0:
                    $('tr#' + target).css({'background-color': '#ffcbcb'});
                    break;
                case 1:
                    $('tr#' + target).css({'background-color': '#c1ffc2'});
                    break;
                case 2:
                    $('tr#' + target).css({'background-color': '#c9c9c9'});
                    break;
            }
            $.post('<?php echo site_url('api/update_user_level'); ?>', {uid: uid, level: level}, function(res) {
                if (res.status === 'success') {
                    alert('อัพเดตสำเร็จแล้ว');
                } else {
                    alert('ไม่สามารถดำเนินตามคำขอ');
                }
            }, 'json');
        });

        $('.prefix_code').keyup(function() {
            var current_string = $(this).val();
            $(this).val(current_string.toUpperCase());
        });

        $('.prefix_update_btn').click(function() {
            var target = "prefix_for_uid_";
            var uid = $(this).metadata().uid;
            var prefix_code = $("#" + target + uid).val().toUpperCase();
            if (!prefix_code) {
                prefix_code = 'ELY';
            }

            $.post('<?php echo site_url('api/update_prefix_code'); ?>', {uid: uid, prefix_code: prefix_code}, function(res) {
                console.log(res);
            }, 'json');

        });
    });
</script>