<div class="span9">
    <legend>
        <h3>Order ID :                         
            <span id="this_order_id"><?php echo $this->convert_id->order_id($order->order_id, $order->order_date); ?></span>
        </h3>
    </legend>
    <div class="row-fluid">
        <?php echo $order_info; ?>
    </div>
    <input type="hidden" id="transport_date" value="<?php echo $order->transport_date; ?>">
</div>