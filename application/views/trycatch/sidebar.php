<div class="span3">
    <ul class="nav nav-list well side-menu">
        <li class="nav-header">The ElysiumGarden System</li>
        <li><a href="<?php echo site_url('trycatch/welcome'); ?>">Home</a></li>
        <li class="divider"></li>
        <li class="nav-header">Order</li> 
        <li>
            <a href="<?php echo site_url('trycatch/order'); ?>">
                <?php $order = $this->getdata->get_type_order(0); ?>
                <?php $number = count($order); ?>
                <?php if ($number > 0): ?>
                    <span class="badge badge-important" style="text-indent: 0px;"><?php echo $number; ?></span>
                <?php endif; ?>
                คำสั่งซื้อเข้ามาใหม่
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('trycatch/process'); ?>">
                <?php $order = $this->getdata->get_type_order(1); ?>
                <?php $number = count($order); ?>
                <?php if ($number > 0): ?>
                    <span class="badge badge-important" style="text-indent: 0px;"><?php echo $number; ?></span>
                <?php endif; ?>
                เตรียมสินค้าในการจัดส่ง
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('trycatch/credit'); ?>">
                <?php $order = $this->getdata->get_type_order(2); ?>
                <?php $number = count($order); ?>
                <?php if ($number > 0): ?>
                    <span class="badge badge-important" style="text-indent: 0px;"><?php echo $number; ?></span>
                <?php endif; ?>
                ส่งเรียบร้อย
            </a>
        </li>
        <li>
            <a href="<?php echo site_url('trycatch/paid'); ?>">
                <?php $order = $this->getdata->get_type_order(3); ?>
                <?php $number = count($order); ?>
                <?php if ($number > 0): ?>
                    <span class="badge badge-important" style="text-indent: 0px;"><?php echo $number; ?></span>
                <?php endif; ?>
                ชำระเงินเรียบร้อย
            </a>
        </li>
        <li class="divider"></li>  
        <li class="nav-header">Product</li>
        <li><a href="<?php echo site_url('trycatch/product'); ?>">All Product</a></li>
        <li><a href="<?php echo site_url('trycatch/new_product'); ?>">New Product</a></li>
        <li class="divider"></li>
        <li class="nav-header">Promotion</li>
        <li><a href="<?php echo site_url('trycatch/sms'); ?>">New SMS</a></li>
        <li><a href="<?php echo site_url('trycatch/newsletter'); ?>">NewsLetter</a></li>
        <li class="divider"></li>     
        <li class="nav-header">Gallery</li>
        <li><a href="<?php echo site_url('trycatch/gallery'); ?>">All Album</a></li>
        <li><a href="<?php echo site_url('trycatch/add_gallery'); ?>">New Album</a></li>
        <li class="divider"></li>
        <li class="nav-header">Member</li>
        <li><a href="<?php echo site_url('trycatch/member'); ?>">All Member</a></li>
        <?php $list = $this->auth->get_waiting_user_list(); ?>
        <?php if (count($list)): ?>
            <li>
                <a href="<?php echo site_url('trycatch/waitinglist'); ?>">Waiting List 
                    <span class="badge badge-important" style="text-indent: 0px;"><?php echo count($list) ?></span>
                </a>
            </li>
        <?php endif; ?>
        <li><a href="<?php echo site_url('trycatch/banned'); ?>">Banned</a></li>
        <li class="divider"></li>
        <li class="nav-header">Email Manager</li>
        <li><a href="<?php echo site_url('trycatch/email'); ?>">Email list</a></li>
        <li class="divider"></li>
        <li class="nav-header">SMS Manager</li>
        <li><a href="<?php echo site_url('trycatch/telephone'); ?>">Telephone number list</a></li>
    </ul>
</div>