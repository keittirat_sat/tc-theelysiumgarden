<div class="span9">
    <legend><h3><?php echo $title; ?></h3></legend>
    <div class="row-fluid">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="text-align: center;">#</th>
                    <th style="text-align: center;">Company / Name</th>
                    <th style="text-align: center;">Order Date</th>
                    <th style="text-align: center;">Credit Date</th>
                    <th style="text-align: center;">Total</th>
                    <th style="text-align: center;">View</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($order)): ?>
                    <?php foreach ($order as $key => $rec): ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $key + 1; ?></td>
                            <td style="text-align: right;"><?php echo $rec->name; ?></td>
                            <td><?php echo date('d-m-Y H:i:s', $rec->order_date); ?></td>
                            <td><?php echo date('d-m-Y H:i:s', $rec->credit_date); ?></td>
                            <td style="text-align: right;">฿<?php echo $rec->total; ?></td>
                            <td style="text-align: center;"><a href="<?php echo site_url('trycatch/view_order/' . $rec->order_id); ?>" class="btn btn-info">VIEW DETAIL</a></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="6">
                            <h4 class="txt_center">ไม่พบข้อมูล</h4>
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>