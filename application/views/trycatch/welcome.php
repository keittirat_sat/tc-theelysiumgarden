<div class="span9">
    <legend><h3>Web statistics by Google Analytics</h3></legend>
    <div class="accordion" id="accordion">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Web Statistics
                </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <div class="well">
                        <p><b>Period :</b> <?php echo "{$visitor_pageview['summary']->startDate} ~ {$visitor_pageview['summary']->endDate}"; ?></p>
                        <?php if (array_key_exists('pageviews', $visitor_pageview)): ?>
                            <p><b>Pageviews :</b> <?php echo "{$visitor_pageview['pageviews']} Time"; ?></p>
                        <?php endif; ?>
                        <?php if (array_key_exists('visits', $visitor_pageview)): ?>
                            <p><b>Visitor :</b> <?php echo "{$visitor_pageview['visits']} People"; ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    Google Keyword
                </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="span1" style="text-align: center;">#</th>
                                    <th class="span9">Keyword</th>
                                    <th class="span2" style="text-align: center;">Visitor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (count($keyword) > 1): ?>
                                    <?php $index = 0; ?>
                                    <?php foreach ($keyword as $i => $v): ?>
                                        <?php if ($i != "summary"): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $index; ?></td>
                                                <td><a href="<?php echo "http://www.google.co.th/search?q=" . urldecode($i); ?>" target="_blank"><?php echo $i; ?></a></td>
                                                <td style="text-align: center;"><?php echo $v->visits; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php $index++; ?>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="3">ไม่มีข้อมูล</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    Most Site Referrer
                </a>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="span1" style="text-align: center;">#</th>
                                <th class="span5">Site</th>
                                <th class="span2" style="text-align: center;">Behavior</th>
                                <th class="span2" style="text-align: center;">Visitor</th>
                                <th class="span2" style="text-align: center;">Bounces</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($site_referrer) > 1): ?>
                                <?php $index = 0; ?>
                                <?php foreach ($site_referrer as $i => $v): ?>
                                    <?php if ($i != "summary"): ?>
                                        <?php foreach ($v as $i_lv2 => $v_lv2): ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $index; ?></td>
                                                <td><?php echo $i; ?></td>
                                                <td style="text-align: center;"><?php echo $i_lv2; ?></td>
                                                <td style="text-align: center;"><?php echo $v_lv2->visits; ?></td>
                                                <td style="text-align: center;"><?php echo $v_lv2->bounces; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php $index++; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5">ไม่มีข้อมูล</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>