<div class="span9">
    <legend><h3>New Product</h3></legend>
    <div class="row-fluid">
        <div class="span8">
            <div class="accordion" id="accordion">
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            English
                        </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                        <div class="accordion-inner">
                            <div class="row-fluid">
                                <input type="text" id="title_en" for="product_name_en" name="title_en" class="span12" placeholder="Product's name" value="<?php echo $pname_en ?>">
                                <textarea class='span12' name='msg_en' placeholder="Product's detail" for="product_detail_en"><?php echo $pdetail_en; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            ภาษาไทย
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <div class="row-fluid">
                                <input type="text" id="title_th" for="product_name_th" name="title_th" class="span12" placeholder="ชื่อผลิตภัณฑ์" value="<?php echo $pname_th ?>">
                                <textarea class='span12' name='msg_th' placeholder='รายละเอียดสินค้า' for="product_detail_th"><?php echo $pdetail_th; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <form action='<?php echo site_url('api/upload'); ?>' method='post' id='image_form' enctype="multipart/form-data">
                <div class="well">
                    <label>Feature Image</label>
                    <div class="progress progress-striped active" style="margin-bottom: 10px;">
                        <div class="bar" style="width: 40%;"></div>
                    </div>
                    <button class="btn btn-info upload_btn" style="width: 190px; position: relative;">
                        UPLOAD IMAGE
                        <input type='file' name='myimage' id='file_upload'>
                    </button>
                    <div class="thumbnail-img" style="margin-top: 10px;">
                        <?php if ($img_url): ?>
                            <img src="<?php echo $img_url; ?>" width='100%'>
                        <?php endif; ?>
                    </div>
                    <input type="hidden" name="folder" value="<?php echo $post_id; ?>">
                </div>
            </form>

            <form action='<?php echo site_url('api/update_post'); ?>' method='post' id="form_product_upload">
                <div class="well">
                    <label>จำนวน</label>
                    <input type="text" name="quantity" placeholder="QUANTITY" style="width: 177px;" value="<?php echo $pquantity; ?>">
                    <label>ราคา</label>
                    <input type="text" name="price" placeholder="PRICE" style="width: 177px;" value="<?php echo $price; ?>">
                    <select name="category" style="width: 191px;">
                        <option value="flower" <?php echo ($cate == "flower")?"selected":"";?>>Flower</option>
                        <option value="fruit" <?php echo ($cate == "fruit")?"selected":"";?>>Fruit</option>
                        <option value="vagetable" <?php echo ($cate == "vagetable")?"selected":"";?>>Vagetable</option>
                    </select>
                    <button class="btn btn-primary" type='submit' style="width: 190px; margin-bottom: 10px;">UPDATE</button>
                    <button class="btn btn-danger del-btn" type='button' style="width: 190px;">DELETE</button>
                </div>
                <input type='hidden' name='product_name_en' value='<?php echo $pname_en; ?>'>
                <input type='hidden' name='product_name_th' value='<?php echo $pname_th; ?>'>
                <input type='hidden' name='product_detail_th' value='<?php echo $pdetail_th; ?>'>
                <input type='hidden' name='product_detail_en' value='<?php echo $pdetail_en; ?>'>
                <input type='hidden' name='product_image' value='<?php echo $pthumbnail; ?>'>
                <input type='hidden' name='post_type' value="<?php echo $post_type; ?>">
                <input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
            </form>
        </div>
    </div>
</div>
<style>
    textarea{
        resize: none;
        height: 350px;
    }

    .accordion-heading{
        background-color: #efefef;
        text-shadow: 1px 1px 3px #fff;
    }

    #file_upload{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        z-index: 99;
        opacity: 0;
    }
</style>
<script type="text/javascript">
    $(function() {
        $('.progress').hide();
        $('[type=number]').change(function() {
            var number = $(this).val();
            number = parseFloat(number);
            if (number < 0) {
                $(this).val(0);
            }
        });

        $('#file_upload').change(function() {
            console.log($(this).val());
            $('form#image_form').submit();
        });

        $('[type=text]').change(function() {
            var target = $(this).attr('for');
            var data = $(this).val();

            $('[name=' + target + ']').val(data);
            console.log(target, data);
        });

        $('textarea').change(function() {
            var target = $(this).attr('for');
            var data = $(this).val();

            $('[name=' + target + ']').val(data);
            console.log(target, data);
        });

        $('form#image_form').ajaxForm({
            beforeSend: function()
            {
                $('.progress').show();
                $('.bar').css({width: "0%"});
                $('.upload_btn').hide();
                $('.thumbnail-img').html("").hide();
            },
            uploadProgress: function(event, position, total, percentComplete)
            {
                $('.bar').css({width: percentComplete + "%"});
            },
            complete: function(xhr)
            {
//                console.log(xhr);
                var json = $.parseJSON(xhr.responseText);
//                console.log(json);
                $('.progress').hide();
                $('.upload_btn').show();
                var img = "<img src='" + json.upload_data.url + "' width='100%'>";
                $('.thumbnail-img').html(img).show();
                $('[name=product_image]').val(json.upload_data.img_id);
            }
        });

        $('form#form_product_upload').ajaxForm({
            complete: function(xhr) {
                console.log(xhr.responseText);
                var json = $.parseJSON(xhr.responseText);
                location.href = json.url;
            }
        });

        $('.del-btn').click(function() {
            if (confirm("ต้องการลบหรือไม่")) {
                $.get("<?php echo site_url('api/del_post'); ?>", {pid: <?php echo $post_id; ?>}, function() {
                    location.href = "<?php echo site_url('trycatch/product'); ?>";
                });
            }
        });
    });
</script>