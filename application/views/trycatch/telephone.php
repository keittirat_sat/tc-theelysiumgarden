<div class="span9">
    <legend><h3>Telephone list</h3></legend>
    <div class="row-fluid">
        <div class="span8 alpha">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="20%">Category</th>
                        <th width="70%">Telephone</th>
                    </tr>
                </thead>
                <tbody id="display_table">
                    <?php if (count($custom)): ?>
                        <?php foreach ($custom as $key => $rec): ?>
                            <tr>
                                <td style="vertical-align: middle; text-align: center;"><?php echo ($key + 1); ?></td>
                                <td style="vertical-align: middle;"><?php echo $rec->ctable; ?></td>
                                <td>
                                    <form action="<?php echo site_url('api/update_custom'); ?>" style="margin: 10px 0px 0px;" class="form_update" method="post">
                                        <div class="input-append">
                                            <input class="span6" name="cvalue" type="text" value="<?php echo $rec->cvalue; ?>">
                                            <button class="btn btn-primary" type="submit">UPDATE</button>
                                            <input type="hidden" value="<?php echo $rec->cid; ?>" name="cid">
                                            <input type="hidden" value="telephone" name="cname">
                                            <button class="btn btn-danger del-btn {cid:<?php echo $rec->cid; ?>}" type="button" <?php echo ($rec->ctable == 'users') ? "disabled" : ""; ?>>DELETE</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="3"><h4 style="text-align: center;">ไม่พบเบอร์โทรศัพท์</h4></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="span4">
            <div class="well">
                <form action="<?php echo site_url('api/add_custom'); ?>" method="post" id="add_custom_field">
                    <label>Add new Telephone</label>
                    <div class="input-append">
                        <input class="span8" type="text" name="cvalue" placeholder="New email">
                        <button class="btn btn-success" type="submit">Add</button>
                    </div>
                    <input type="hidden" value="telephone" name="cname">
                    <input type="hidden" value="custom" name="ctable">
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    th{
        text-align: center !important;
    }
</style>
<script>
    $(function() {
        $('form.form_update').ajaxForm({
            complete: function(xhr) {
                var str = xhr.responseText;
                var json = $.parseJSON(str);
                var custom = json.custom;
                if (custom.length > 0) {
                    location.reload();
                }
            }
        });


        $('form#add_custom_field').ajaxForm({
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.reload();
                } else {
                    alert("ไม่สามารถเพิ่มข้อมูล");
                }
            }
        });
        
        $('.del-btn').click(function(){
            var cid = $(this).metadata().cid;
            $.post('<?php echo site_url('api/del_custom'); ?>',{cid: cid},function(res){
                if(res.status === "success"){
                    location.reload();
                } else {
                    alert("ไม่สามารถเพิ่มข้อมูล");
                }
            },'json');
        });
    });
</script>