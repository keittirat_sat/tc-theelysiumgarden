<div class='span9'>
    <legend><h3>Waiting List</h3></legend>
    <div class='row-fluid'>
        <div class='span12'>

            <table class='table table-condensed'>
                <thead>
                    <tr>
                        <th class='txt_center'>#</th>
                        <th class='txt_center'>Company / Name</th>
                        <th class='txt_center'>Username</th>
                        <th class='txt_center'>Address</th>
                        <th class='txt_center'>Class</th>
                        <th class='txt_center'>Approve</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($userlist as $key => $rec): ?>
                        <tr>
                            <td class='txt_center'><?php echo $key + 1; ?></td>
                            <td><?php echo $rec->name; ?></td>
                            <td><?php echo $rec->username; ?></td>
                            <td>
                                <?php echo $rec->address1 . (($rec->address2) ? "<br/>" . $rec->address2 : "") . "<br/>" . $rec->district . "<br/>" . $rec->province . " " . $rec->zip; ?>
                            </td>
                            <td class='txt_center'><?php echo ($rec->level == 0) ? "admin" : "normal"; ?></td>
                            <td class='txt_center'>
                                <button class='btn btn-warning approve_btn unapprove {uid:<?php echo $rec->uid ?>}'>APPROVE</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<script type='text/javascript'>
    $(function() {
        $('.approve_btn').click(function() {
            var uid = $(this).metadata().uid;
            var current = $(this);
            $.get('<?php echo site_url('api/approve_member'); ?>/' + uid, function(res) {
                console.log(res);
                if (res.status === "success") {
                    $(current).removeClass('unapprove');
                    var count_unapprove = $('.unapprove').length;
                    if (count_unapprove === 0) {
                        location.href = "<?php echo site_url('trycatch/member'); ?>";
                    } else {
                        console.log(count_unapprove);
                    }
                } else {
                    alert("ไม่สามารถดำเนินการตามคำขอได้");
                }
            },'json');
        });
    });
</script>