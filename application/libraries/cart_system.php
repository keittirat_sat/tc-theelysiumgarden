<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart_system {

    private $ci;
    private $cart;

    public function __construct() {
        $this->ci = get_instance();
        $this->initial_cart();
    }

    private function initial_cart() {
        $this->cart = $this->ci->session->userdata('cart');
        if (empty($this->cart)) {
            $this->cart = array();
        }
    }

    public function add_cart($pid, $total) {
        if (array_key_exists($pid, $this->cart)) {
            $this->cart[$pid] += $total;
        } else {
            $this->cart[$pid] = $total;
        }
        return $this->update_state();
    }

    public function update_cart($pid, $total) {
        $this->cart[$pid] = $total;
        return $this->update_state();
    }

    public function retrive_cart() {
        $cart_log = array();
        foreach ($this->cart as $pid => &$quantity) {
            $data = $this->ci->getdata->get_post($pid);
            $temp = array(
                'pid' => $data->pid,
                'name' => json_decode($data->pname),
                'price' => $data->price,
                'priece' => $quantity,
                'category' => $data->category,
                'total' => $data->price * $quantity
            );
            array_push($cart_log, $temp);
        }
        return $cart_log;
    }

    public function update_state() {
        $log = $this->check_quatity();
        $this->ci->session->set_userdata(array('cart' => $this->cart));
        return $log;
    }

    public function check_quatity() {
        $log['status'] = "success";
        $log['list'] = array();
        $log['total_type_of_product'] = 0;
        foreach ($this->cart as $pid => &$quantity) {
            $data = $this->ci->getdata->get_post($pid);
            $log['total_type_of_product']++;
            if ($data->pquantity < $quantity) {
                $log['status'] = "error";
                array_push($log['list'], json_decode($data->pname));
                $quantity = $data->pquantity;
            }
        }
        return $log;
    }

    public function clear_cart() {
        $this->ci->session->set_userdata(array('cart' => null));
    }

//    public function sent_order($order_id, $uid, $total) {
//        $today = date('U');
//        $expire = $today + 1296000;
//        $temp = array(
//            'order_id' => $order_id,
//            'uid' => $uid,
//            'order_date' => $today,
//            'credit_date' => $expire,
//            'total' => $total
//        );
//        
//        return $this->ci->getdata->insert_order($temp);
//    }

    public function sent_order($uid, $total) {
        $today = date('U');
        $temp = array(
            'uid' => $uid,
            'order_date' => $today,
            'total' => $total
        );

        return $this->ci->getdata->insert_order($temp);
    }

}

?>
