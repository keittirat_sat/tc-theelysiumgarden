<?php

class Email_server {

    private $ci;
    private $bcc_list;
    public $owner;

    public function __construct() {
        $this->ci = get_instance();
        $this->owner = "frankent@gmail.com";
        $this->bcc_list = array("keittirat@gmail.com", "theelysiumgarden.service@gmail.com");
    }

    public function email_send($name, $subject, $email, $detail, $recv, $attach = array()) {
        return $this->server_mail($name, $subject, $email, $detail, $recv, $attach);
    }

    public function email_order($transaction_id, $new_order = true, $attach = array()) {
        $order = $this->ci->getdata->get_order_by_id($transaction_id);
        $print_order_id = $this->ci->convert_id->order_id($transaction_id, $order->order_date);
        if ($new_order) {
            $subject = "Your new order #{$print_order_id}";
        } else {
            $subject = "The Elysium Garden #{$print_order_id} update status [" . date('d F Y H:i:s') . "]";
        }

        $order_user = $this->ci->auth->check_uid($order->uid);
        $order_item = $this->ci->getdata->get_order_list($order->order_id);

        $msg = $this->ci->template->element('email_template', array('order' => $order, 'order_user' => $order_user, 'order_item' => $order_item, 'can_edit' => false));
        $this->email_send("The Elysium Garden", $subject, $order_user[0]->email, $msg, array($order_user[0]->email), $attach);
    }
    
    private function cnx_sendmail($name, $subject, $email, $detail, $recv, $attach = array()) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'theelysiumgarden.service@gmail.com',
            'smtp_pass' => 'gdiupo500510xxx',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );

        $this->ci->email->initialize($config);

        $this->ci->email->from("theelysiumgarden.service@gmail.com", "The Elysium Garden Service");

        if (!empty($email)) {
            $this->ci->email->reply_to($email, $name);
        }

        if (!empty($attach)) {
            foreach ($attach as $file) {
                $this->ci->email->attach($file);
            }
        }

        $this->ci->email->to($recv);
        $this->ci->email->bcc($this->bcc_list);

        $this->ci->email->subject($subject);

        $this->ci->email->message($detail);
        return $this->ci->email->send();
    }

    private function server_mail($name, $subject, $email, $detail, $recv, $attach = array()) {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';

        $this->ci->email->initialize($config);

        $this->ci->email->from("contact@trycatch.in.th", "TryCatch Service");
        if (!empty($email)) {
            $this->ci->email->reply_to($email, $name);
        }

        if (!empty($attach)) {
            foreach ($attach as $file) {
                $this->ci->email->attach($file);
            }
        }

        $this->ci->email->to($recv);
        //$this->email->to(array('keittirat@gmail.com'));
        $this->ci->email->bcc($this->bcc_list);

        $this->ci->email->subject($subject);
        $this->ci->email->message($detail);

        return $this->ci->email->send();
    }

}
?>
