<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Convert_id {

    private $ci;

    public function __construct() {
        $this->ci = get_instance();
    }

    public function product_id($id, $cate) {
        switch ($cate) {
            case 'fruit':
                $mode = "Po";
                break;
            case 'vagetable':
                $mode = "Ve";
                break;
            case 'flower':
                $mode = "Fo";
                break;
        }
        return "{$mode}-{$id}";
    }

    public function order_id($id, $date = null, $forfolder = false) {
        $print_id = str_pad($id, 6, "0", STR_PAD_LEFT);
        if ($date == null) {
            $date = date('U');
        }
        $year = date('Y', $date) + 543;
        $year = strval($year);
        if ($forfolder) {
            $name = "POS-{$print_id}_{$year[2]}{$year[3]}";
        } else {
            $name = "POS-{$print_id}/{$year[2]}{$year[3]}";
        }
        return $name;
    }

    public function get_prefix_code_by_uid($uid) {
        $data = $this->ci->db->select('prefix_code')->from('users')->where('uid', $uid)->get();
        $data = $data->result();
        $data = $data[0];
        return $data->prefix_code;
    }

    public function user_id($id) {
        return "{$this->get_prefix_code_by_uid($id)}-" . str_pad($id, 5, "0", STR_PAD_LEFT);
    }

    public function cal_date_diff($start, $end) {
        $perday = 60 * 60 * 24;
        $diff = $end - $start;
        return ceil($diff / $perday);
    }

    public function export_pdf($transaction_id) {
        //Setup Page
        $this->ci->mpdf->mpdf('th', '', 0, '', 5, 5, 5, 0, 0, 0, 'P');

        //Prepare data
        $order = $this->ci->getdata->get_order_by_id($transaction_id);
        $order_user = $this->ci->auth->check_uid($order->uid);
        $order_item = $this->ci->getdata->get_order_list($order->order_id);

        //Path config
        $this_order_id = $this->order_id($order->order_id, $order->order_date, true);
        $path = "./pdf/" . $this_order_id;
        $url = site_url("pdf/{$this_order_id}");
        if (!is_dir($path)) {
            mkdir($path);
        }

        switch ($order->status) {
            case 0: $prefix = "[New_Order]";
                break;
            case 1: $prefix = "[Proceed]";
                break;
            case 2: $prefix = "[Shipped]";
                break;
            case 3: $prefix = "[Paid]";
                break;
        }

        $suffix = date('U');
        $mod .= $path . "/{$prefix}{$this_order_id}_v{$suffix}.pdf";

        $path .= "/{$prefix}{$this_order_id}.pdf";
        if (is_file($path)) {
            //if exist don't delete, rename it
            rename($path, $mod);
        }

        //Read form
        $msg = $this->ci->template->element('order_pdf', array('order' => $order, 'order_user' => $order_user, 'order_item' => $order_item, 'can_edit' => false));
        $this->ci->mpdf->WriteHTML($msg);
        $this->ci->mpdf->Output($path);
        $this->ci->mpdf->close();

        return $path;
    }

    public function calculate_date_offset() {
        
    }

}

?>
