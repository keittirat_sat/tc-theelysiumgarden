<?php
/* This Library use in TryCatch Project Only.
 * Standard Query from Google Analytic API Doc
 * Go to -> https://developers.google.com/analytics/resources/articles/gdataCommonQueries
 * Go to this link for more information -> https://developers.google.com/analytics/devguides/reporting/core/v2/gdataReferenceDataFeed
 * 
 * Require Codeigniter Library to connect Google Analytic API
 * Go to -> https://github.com/chonthu/Codeigniter-Google-Analytics-Class
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Site_analytic {

    private $ci;
    private $ga;

    public function __construct() {
        // get CI instance
        $this->ci = &get_instance();

        //GET ga_api
        $this->ga = $this->ci->ga_api->login();
    }

    public function get_visitor_pageview($limit = 100) {
        return $this->ga->metric('visits,pageviews')->sort_by('visits')->limit($limit)->get_object();
    }

    public function get_user_referrer($limit = 100) {
        return $this->ga->dimension('source, medium')->metric('visits,bounces')->sort_by('visits')->limit($limit)->get_object();
    }

    public function get_organic_search($limit = 100) {
//        dimensions = ga:source
//        metrics = ga:pageviews, ga:timeOnSite, ga:exits
//        filters = ga:medium==organic
//        sort = -ga:pageviews
        return $this->ga->dimension('source')->metric('pageviews,timeOnSite,exits')->sort_by('pageviews')->limit($limit)->get_object();
    }

    public function get_keyword($limit = 100) {
//        dimensions = ga:keyword
//        metrics = ga:visits
        return $this->ga->dimension('keyword')->metric('visits')->sort_by('visits')->limit($limit)->get_object();
    }

}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */