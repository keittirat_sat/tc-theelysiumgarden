<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of block_generator
 * Generate block in The Elysium Garden Project in frontend
 * @author trycatch
 */
class Block_generator {

    private $ci;

    function __construct() {
        $this->ci = get_instance();
    }

    function get_product_from($rec, $grid = 3, $counter = 0, $lang = 'en') {
        $name = json_decode($rec->pname);
        $detail = json_decode($rec->pdetail);
        ?>
        <div class="<?php echo "span{$grid}"; ?> set_popover <?php echo ($counter % (12 / $grid)) ? "" : "alpha"; ?>" style="margin-top:15px; margin-bottom: 15px;" title="<?php echo ($lang == 'en') ? $name->en : $name->th; ?>" data-content="<?php echo nl2br(($lang == 'en') ? $detail->en : $detail->th); ?>">
            <a class="thumbnail trycatch_thumbnail" style="background-image: url('<?php echo $rec->img_url; ?>');"></a>
            <h4 class="txt_center white diavlo title-product">
                <?php echo ($lang == 'en') ? $name->en : $name->th; ?>
            </h4>
            <?php echo ($this->ci->auth->is_loged()) ? "<h5 class='txt_center white diavlo'> ฿{$rec->price} / {$rec->pquantity} in stock</h5>" : ""; ?>
            <?php if ($this->ci->auth->is_loged()): ?>
                <?php if ($rec->pquantity > 0): ?>
                    <!--Order box-->
                    <p class="txt_center white">
                        <span style="display:block;"><a href="#more" class="add_more_quantity {pid:<?php echo $rec->pid ?>, target:'<?php echo "product_{$rec->pid}"; ?>'}"><i class="icon icon-white icon-chevron-up"></i></a></span>
                        <span class="amount_label">Amount : </span>
                        <input type="text" max="<?php echo $rec->pquantity; ?>" class="span4 margin-bottom-none number <?php echo "product_{$rec->pid}"; ?>" target="<?php echo "product_{$rec->pid}"; ?>" value="0">
                        <button class="btn btn-success add_cart_btn  {pid:<?php echo $rec->pid ?>, target:'<?php echo "product_{$rec->pid}"; ?>'}" data-loading-text="Adding...">SUBMIT</button>
                        <span style="display:block;"><a href="#desc" class="de_more_quantity {pid:<?php echo $rec->pid ?>, target:'<?php echo "product_{$rec->pid}"; ?>'}"><i class="icon icon-white icon-chevron-down"></i></a></span>
                    </p>
                    <!--End Order box-->
                <?php else: ?>
                    <p class="txt_center red">
                        <b>
                            <?php if ($lang == "th"): ?>
                                ขออภัยสินค้าหมดชั่วคราว
                            <?php else: ?>
                                OUT OF STOCK
                            <?php endif; ?>
                        </b>
                    </p>
                <?php endif; ?>
            <?php endif; ?>
        </div> 
        <?php
    }

}
?>
