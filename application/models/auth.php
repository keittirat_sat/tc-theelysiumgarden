<?php

class Auth extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function login($username, $password, $level = 1) {
        $data_record = $this->ci->db->from('users')->where('username', $username)->where('password', do_hash($password, 'md5'))->where('level', $level)->where('approve', 1)->get();
        return $data_record->result();
    }

    public function admin_login($username, $password) {
        return $this->login($username, $password, 0);
    }

    public function take_login($data) {
        if (count($data)) {
            $data = $data[0];
            $this->ci->session->set_userdata('userinfo', $data);
            return true;
        }
        return false;
    }

    public function approve_member($uid) {
        if (count($this->check_uid($uid))) {
            return $this->ci->db->where('uid', $uid)->update('users', array('approve' => 1));
        } else {
            return false;
        }
    }

    public function check_uid($uid) {
        $data = $this->ci->db->from('users')->where('uid', $uid)->get();

        $user = $data->result();
        if (count($user)) {
            $tel = $this->ci->getdata->get_custom_val('telephone', $order = 'desc', $uid, 'users');
            $tel = $tel[0];

            $email = $this->ci->getdata->get_custom_val('email', $order = 'desc', $uid, 'users');
            $email = $email[0];

            $fax = $this->ci->getdata->get_custom_val('fax', $order = 'desc', $uid, 'users');
            $fax = $fax[0];

            $user[0]->telephone = $tel->cvalue;
            $user[0]->email = $email->cvalue;
            $user[0]->fax = $fax->cvalue;
        }

        return $user;
    }

    public function check_username($username) {
        $data = $this->ci->db->from('users')->where('username', $username)->get();
        return $data->result();
    }

    public function get_user_info() {
        return $this->ci->session->userdata('userinfo');
    }

    public function get_waiting_user_list() {
        return $this->get_status_user();
    }

    public function get_approve_user_list() {
        return $this->get_status_user(1);
    }

    public function get_status_user($approve = 0) {
        $data = $this->ci->db->from('users')->where('approve', $approve)->get();
        return $data->result();
    }

    public function get_user_by_level($level = 0) {
        $data = $this->ci->db->from('users')->where('level', $level)->get();
        return $data->result();
    }

    public function update_user_level($level, $uid) {
        return $this->ci->db->where('uid', $uid)->update('users', array('level' => $level));
    }

    public function is_loged($level = 'normal') {
        $data = $this->get_user_info();
        if ($data) {
            switch ($level) {
                case 'normal' : return true;
                case 'admin':
                    if ($data->level == 0) {
                        return true;
                    }
                    return false;
            }
        }
        return false;
    }

    public function remove_session() {
        $this->ci->session->unset_userdata('userinfo');
    }

    public function regist($data) {
        return $this->ci->db->insert('users', $data);
    }

    public function update_user_info($uid, $data = array()) {
        if (!empty($data)) {
            $this->ci->getdata->update_custom_val_by_cname(array('cvalue' => $data['email']), 'email', 'users', $uid);
            $this->ci->getdata->update_custom_val_by_cname(array('cvalue' => $data['telephone']), 'telephone', 'users', $uid);
            $this->ci->getdata->update_custom_val_by_cname(array('cvalue' => $data['fax']), 'fax', 'users', $uid);
            unset($data['email']);
            unset($data['telephone']);
            unset($data['fax']);
            return $this->ci->db->where('uid', $uid)->update('users', $data);
        }
        return false;
    }

    public function change_password($data) {
        $current_password = do_hash($data['password'], 'md5');
        $current_session = $this->get_user_info();
        $uid = $current_session->uid;
        $change_to = null;
        if ($data['new_password'] === $data['new_password2']) {
            $change_to = do_hash($data['new_password'], 'md5');
            if ($current_session->password == $current_password) {
                $update = array(
                    'password' => $change_to
                );
                if($this->ci->db->where('uid', $uid)->update('users', $update)){
                    $this->remove_session();
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        return false;
    }

}

?>
