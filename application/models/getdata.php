<?php

class Getdata extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function insert_img($data) {
        return $this->ci->db->insert('image', $data);
    }

    public function get_last_img() {
        $img = $this->ci->db->from('image')->order_by('img_id', 'desc')->limit(1)->get();
        $img = $img->result();
        return $img[0];
    }

    public function get_img_by_id($id) {
        $img = $this->ci->db->from('image')->where('img_id', $id)->get();
        $img = $img->result();
        return $img[0];
    }

    public function get_img_by_post_id($post_id) {
        $img = $this->ci->db->from('image')->where('post_id', $post_id)->get();
        return $img->result();
    }

    public function insert_post($data) {
        $pquantity = $data['pquantity'];
        $cate = $data['category'];
        $price = $data['price'];
        unset($data['pquantity']);
        unset($data['category']);
        unset($data['price']);
        $this->add_custom_val(array(
            'ctable' => 'product',
            'cname' => 'pquantity',
            'cvalue' => $pquantity,
            'belong_id' => $data['pid']
        ));

        $this->add_custom_val(array(
            'ctable' => 'product',
            'cname' => 'category',
            'cvalue' => $cate,
            'belong_id' => $data['pid']
        ));

        $this->add_custom_val(array(
            'ctable' => 'product',
            'cname' => 'price',
            'cvalue' => $price,
            'belong_id' => $data['pid']
        ));
        return $this->ci->db->insert('product', $data);
    }

    public function get_post_by_category($cate, $limit = 0, $order = 'desc') {
        $this->ci->db->from('custom')->join('product', 'product.pid = custom.belong_id')->join('image', 'image.img_id = product.pthumbnail')->where('custom.cvalue', $cate)->order_by('product.pid', $order);
        if ($limit != 0) {
            $this->ci->db->limit($limit);
        }
        $data = $this->ci->db->get();
        $temp = $data->result();
        foreach ($temp as &$rec) {
            $rec->img_list = $this->get_img_by_post_id($rec->pid);
            $v = $this->get_custom_val('pquantity', 'desc', $rec->pid, 'product');
            $p = $this->get_custom_val('price', 'desc', $rec->pid, 'product');
            $rec->pquantity = $v[0]->cvalue;
            $rec->price = $p[0]->cvalue;
        }
        return $temp;
    }

    public function get_product_type($cate, $limit = 0, $order = 'desc') {
        $this->ci->db->from('custom')->join('product', 'product.pid = custom.belong_id')->join('image', 'image.img_id = product.pthumbnail')->where('custom.cvalue', $cate)->order_by('product.pid', $order);
        if ($limit != 0) {
            $this->ci->db->limit($limit);
        }
        $data = $this->ci->db->get();
        $temp = $data->result();
        foreach ($temp as &$rec) {
            $rec->img_list = $this->get_img_by_id($rec->pthumbnail);
            $v = $this->get_custom_val('pquantity', 'desc', $rec->pid, 'product');
            $p = $this->get_custom_val('price', 'desc', $rec->pid, 'product');
            $rec->pquantity = $v[0]->cvalue;
            $rec->price = $p[0]->cvalue;
        }
        return $temp;
    }

    public function get_vat() {
        $vat = $this->get_custom_val($cname = 'vat', $order = 'desc', $belong_id = 0, $ctable = 'billing');
        return $vat[0]->cvalue;
    }

    public function update_post($data, $pid) {
        $this->update_custom_val_by_cname(array(
            'cvalue' => $data['pquantity']
                ), 'pquantity', 'product', $pid);

        $this->update_custom_val_by_cname(array(
            'cvalue' => $data['category']
                ), 'category', 'product', $pid);

        $this->update_custom_val_by_cname(array(
            'cvalue' => $data['price']
                ), 'price', 'product', $pid);

        unset($data['category']);
        unset($data['pquantity']);
        unset($data['price']);
        return $this->ci->db->where('pid', $pid)->update('product', $data);
    }

    public function approve_img($post_id) {
        return $this->ci->db->where('post_id', $post_id)->update('image', array('approve' => 1));
    }

    public function get_product_post($type = 'product', $limit = 0) {
        $this->ci->db->from('product')->join('image', 'product.pthumbnail = image.img_id', 'left')->where('post_type', $type)->order_by('product.pid', 'desc');
        if ($limit != 0) {
            $this->ci->db->limit($limit);
        }
        $post = $this->ci->db->get();
        $temp = $post->result();
        foreach ($temp as &$rec) {
            $rec->img_list = $this->get_img_by_post_id($rec->pid);
            $v = $this->get_custom_val('pquantity', 'desc', $rec->pid, 'product');
            $c = $this->get_custom_val('category', 'desc', $rec->pid, 'product');
            $p = $this->get_custom_val('price', 'desc', $rec->pid, 'product');

            $rec->pquantity = $v[0]->cvalue;
            $rec->category = $c[0]->cvalue;
            $rec->price = $p[0]->cvalue;
        }
        return $temp;
    }

    public function get_post($post_id) {
        $post = $this->ci->db->from('product')->join('image', 'product.pthumbnail = image.img_id', 'left')->where('product.pid', $post_id)->get();
        $post = $post->result();
        if (count($post)) {
            $post[0]->img_list = $this->get_img_by_post_id($post_id);
            $v = $this->get_custom_val('pquantity', 'desc', $post[0]->pid, 'product');
            $c = $this->get_custom_val('category', 'desc', $post[0]->pid, 'product');
            $p = $this->get_custom_val('price', 'desc', $post[0]->pid, 'product');

            $post[0]->pquantity = $v[0]->cvalue;
            $post[0]->category = $c[0]->cvalue;
            $post[0]->price = $p[0]->cvalue;
            return $post[0];
        }
        return false;
    }

    public function del_post($pid) {
        $this->ci->db->where('pid', $pid)->delete('product');
        $this->ci->db->where('post_id', $pid)->delete('image');
        $this->del_custom_cname('quantity', 'product', $pid);
        $this->del_custom_cname('category', 'product', $pid);
        $this->del_custom_cname('price', 'product', $pid);
        $folder = './uploads/' . $pid . '/';

        //Clean a shit
        if (file_exists($folder)) {
            delete_files($folder);
        }
    }

    /*
     * Custom Value
     */

    public function get_custom_val($cname, $order = 'desc', $belong_id = 0, $ctable = 'custom') {
        $this->ci->db->from('custom')->where('cname', $cname);
        if ($belong_id != 0) {
            $this->ci->db->where('belong_id', $belong_id);
        }
        if ($ctable != 'custom') {
            $this->ci->db->where('ctable', $ctable);
        }
        $data = $this->ci->db->order_by('cid', $order)->get();
        return $data->result();
    }

    public function add_custom_val($data) {
        return $this->ci->db->insert('custom', $data);
    }

    public function del_custom_val($cid) {
        return $this->ci->db->where('cid', $cid)->delete('custom');
    }

    public function del_custom_cname($cname, $ctable, $belong) {
        return $this->ci->db->where('cname', $cname)->where('belong_id', $belong)->where('ctable', $ctable)->delete('custom');
    }

    public function update_custom_val($data, $cid) {
        return $this->ci->db->where('cid', $cid)->update('custom', $data);
    }

    public function update_custom_val_by_cname($data, $cname, $ctable, $belong) {
        return $this->ci->db->where('cname', $cname)->where('belong_id', $belong)->where('ctable', $ctable)->update('custom', $data);
    }

    public function reduce_sms_credit($total_recv) {
        $credit = $this->getdata->get_custom_val('sms_credit');
        $credit = $credit[0];
        $cid = $credit->cid;
        $cvalue = $credit->cvalue;
        if (($cvalue - $total_recv) > 0) {
            $cvalue-= $total_recv;
            $this->update_custom_val(array(
                'cvalue' => $cvalue
                    ), $cid);
            return true;
        }
        return false;
    }

    public function set_credit($order_id, $credit_date) {
        return $this->ci->db->where('order_id', $order_id)->update('order', array('credit_date' => $credit_date));
    }

    public function reduce_stock($pid, $total) {
        $stock = $this->get_custom_val('pquantity', 'desc', $pid, 'product');
        $stock = $stock[0];
        $cid = $stock->cid;
        $stock = $stock->cvalue;
        $stock -= $total;

        return $this->update_custom_val(array(
                    'cvalue' => $stock
                        ), $cid);
    }

    public function insert_order($data) {
        return $this->ci->db->insert('order', $data);
    }

    public function insert_order_item($order_id, $pid, $amount) {
        return $this->ci->db->insert('order_item', array(
                    'order_id' => $order_id,
                    'pid' => $pid,
                    'amount' => $amount
        ));
    }

    public function get_last_order() {
        $order = $this->ci->db->from('order')->order_by('order_id', 'desc')->limit(1)->get();
        return $order->result();
    }

    public function get_type_order($status) {
        $order = $this->ci->db->from('order')->join('users', 'order.uid = users.uid')->where('order.status', $status)->get();
        return $order->result();
    }

    public function get_order_by_user_id($uid, $order_by = "asc") {
        $order = $this->ci->db->from('order')->where('uid', $uid)->order_by('order_id', $order_by)->get();
        $order = $order->result();
        foreach ($order as &$each_order) {
            $each_order->order_item = $this->get_order_list($each_order->order_id);
        }
        return $order;
    }

    /*Get Order and billing information*/
    public function get_order_by_id($order_id) {
        $order = $this->ci->db->from('order')->join('users', 'order.uid = users.uid')->where('order.order_id', $order_id)->get();
        $order_single = $order->result();
        return $order_single[0];
    }

    /*Get List of product in that's bill*/
    public function get_order_list($order_id) {
        $order = $this->ci->db->from('order_item')->where('order_id', $order_id)->get();
        $temp = $order->result();
        foreach ($temp as &$item) {
            $item->product_detail = $this->get_post($item->pid);
        }
        return $temp;
    }

    public function get_order_status($order_id) {
        $data = $this->ci->db->from('order')->where('order_id', $order_id)->get();
        $data = $data->result();
        return $data[0];
    }

    public function update_order($status, $order_id) {
        $data['status'] = $status;
        $old_product = $this->get_order_status($order_id);
        if ($old_product->transport_date == 0) {
            switch ($status) {
                case 2:
                case 3:
                    $today = date('U');
//                    $expire = $today + 1296000;
//                    $data['credit_date'] = $expire;
                    $data['transport_date'] = $today;
                    break;
            }
        }

        return $this->ci->db->where('order_id', $order_id)->update('order', $data);
    }

    /*
     * Log Newsletter
     */

    /*
     * add_log()
     * param
     */

    public function add_log($dest, $subject, $msg, $type) {
        $date = date('U');
        $data = array(
            'msg' => $msg,
            'destination' => json_encode($dest),
            'type' => $type,
            'subject' => $subject,
            'sent_date' => $date
        );
        $this->ci->db->insert('newsletter_log', $data);
    }

    public function update_prefix_code($uid, $prefix_code) {
        return $this->ci->db->where('uid', $uid)->update('users', array(
                    'prefix_code' => $prefix_code
        ));
    }

    public function get_due_before($day = 7) {
        $now = date('U');
        $margin = ((60 * 60) * 24 ) * $day;
        $query_unix_date = $now - $margin;

        $temp = $this->ci->db->from('order')->join('users', 'order.uid = users.uid')->where('order.credit_date', '> ' . $query_unix_date)->where('order.status', '< 3')->get();
        return $temp->result();
    }

}

?>
