$(function() {
    $(window).bind('scroll', function(e) {
        var offset_top = 0;
        var scrolledY = $(window).scrollTop();

        var cal_offset = scrolledY - offset_top;
        if (cal_offset < 0) {
            cal_offset = 0;
        }

        cal_offset *= -0.6;
//        console.log(cal_offset);

        $('.parallax_section').stop().animate({'margin': '0px'},
        {
            step: function(now, fx) {
//                console.log(now);
                $(fx.elem).css("background-position", "0px " + cal_offset + "px");
            },
            duration: 400
        }
        );
        //$('.section_two_parallax').stop().animate({'padding-left': (cal_offset / 2) + "px", 'padding-right': (cal_offset / 2) + "px",'margin-top': "-" + (cal_offset * 2) + "px"}, 300);
    });
});