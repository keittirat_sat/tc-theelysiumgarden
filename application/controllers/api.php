<?php

class api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
    }

    public function login() {
        $data = $this->input->post();
        $temp = $this->auth->admin_login($data['username'], $data['password']);
        if ($this->auth->take_login($temp)) {
            $data['status'] = 'success';
            $data['url'] = site_url('trycatch/welcome');
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function guest_login() {
        $data = $this->input->post();
        $temp = $this->auth->login($data['username'], $data['password']);
        if ($this->auth->take_login($temp)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function logout() {
        $this->auth->remove_session();
        redirect(site_url());
    }

    public function resize_image($image_path, $size, $new_image = null) {
        //Setup Image manipulation procedure
        $config_x = array();
        //$config['image_library'] = 'gd2';
        $config_x['source_image'] = $image_path;
        if ($new_image) {
            $config_x['new_image'] = $new_image;
        }
        $config_x['create_thumb'] = FALSE;
        $config_x['maintain_ratio'] = TRUE;
        $config_x['width'] = $size;
        $config_x['height'] = $size;

        //clear old config
        $this->image_lib->clear();

        //Re initial
        $this->image_lib->initialize($config_x);

        //Resize
        return $this->image_lib->resize();
    }

    public function upload() {
        $folder = './uploads/' . $this->input->post('folder') . '/';

        //Clean a shit
        if (file_exists($folder)) {
            delete_files($folder);
        }

        @mkdir($folder);
        $config['upload_path'] = $folder;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('myimage')) {
//            Error
            $data = array('error' => $this->upload->display_errors());
        } else {
//            Success
            $response = $this->upload->data();
            $data = array('upload_data' => $response);

            $this->resize_image($data['upload_data']['full_path'], 1024);

            $data['upload_data']['folder'] = $this->input->post('post');
            $data['upload_data']['url'] = site_url("uploads/{$this->input->post('folder')}/{$data['upload_data']['file_name']}");

            $this->getdata->insert_img(array(
                'img_path' => $data['upload_data']['full_path'],
                'img_url' => $data['upload_data']['url'],
                'post_id' => $this->input->post('folder')
            ));

            $res_id = $this->getdata->get_last_img();
            $data['upload_data']['img_id'] = $res_id->img_id;
        }
        echo json_encode($data);
    }

    public function upload_post() {
        $data = $this->input->post();
        $post_data = array(
            'pname' => json_encode(array(
                'en' => $data['product_name_en'],
                'th' => $data['product_name_th']
            )),
            'pdetail' => json_encode(array(
                'en' => $data['product_detail_en'],
                'th' => $data['product_detail_th']
            )),
            'pquantity' => $data['quantity'],
            'pid' => $data['post_id'],
            'pthumbnail' => $data['product_image'],
            'post_type' => $data['post_type'],
            'category' => $data['category'],
            'price' => $data['price']
        );
        $res = $this->getdata->insert_post($post_data);
        if ($res) {
            $response['status'] = 'success';
            $response['url'] = site_url("trycatch/" . (($data['post_type'] == "product") ? "post" : "gallery_edit") . "?post_id={$data['post_id']}");
            $this->getdata->approve_img($data['post_id']);
        } else {
            $response['status'] = 'fail';
        }
        echo json_encode($response);
    }

    public function update_post() {
        $data = $this->input->post();

        $pid = $data['post_id'];
        $post_data = array(
            'pname' => json_encode(array(
                'en' => $data['product_name_en'],
                'th' => $data['product_name_th']
            )),
            'pdetail' => json_encode(array(
                'en' => $data['product_detail_en'],
                'th' => $data['product_detail_th']
            )),
            'pquantity' => $data['quantity'],
            'pthumbnail' => $data['product_image'],
            'post_type' => $data['post_type'],
            'category' => $data['category'],
            'price' => $data['price']
        );

        $res = $this->getdata->update_post($post_data, $pid);

        if ($res) {
            $response['status'] = 'success';
            $response['url'] = site_url("trycatch/" . (($data['post_type'] == "product") ? "post" : "gallery_edit") . "?post_id={$data['post_id']}");
            $this->getdata->approve_img($data['product_image']);
        } else {
            $response['status'] = 'fail';
        }
        echo json_encode($response);
    }

    public function del_post() {
        $pid = $this->input->get_post('pid');
        $this->getdata->del_post($pid);
    }

    public function upload_album_image() {
        $folder = './uploads/' . $this->input->post('post_id') . '/';
        if (!is_dir($folder)) {
            mkdir($folder);
        }

        chmod($folder, 0777);

        $data = array();
        foreach ($_FILES['myimage']['tmp_name'] as $file) {
            array_push($data, $file);
        }

        $desc = array();
        foreach ($_FILES['myimage']["name"] as $file) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            array_push($desc, md5($file) . "." . $ext);
        }

        for ($i = 0; $i < count($data); $i++) {

            $image_desc = $folder . "/" . $desc[$i];
//            $image_desc_thumb = $path . "/mini_" . $desc[$i];

            if (is_file($image_desc)) {
                unlink($image_desc);
            }

            move_uploaded_file($data[$i], $image_desc);
            //chmod($image_desc, 0777);
            if (is_file($image_desc)) {
                //Create Thumbnail
//                $this->resize_image($image_desc, 250, $image_desc_thumb);
                //Store in DB
//                $this->post->insert_img(array('img_name' => $desc[$i], 'album_id' => $album_id));
                $this->getdata->insert_img(array(
                    'img_path' => $image_desc,
                    'img_url' => site_url("uploads/{$this->input->post('post_id')}/{$desc[$i]}"),
                    'post_id' => $this->input->post('post_id')
                ));

                //Resize image
                $this->resize_image($image_desc, 1024);
            }
        }

        $img = $this->getdata->get_img_by_post_id($this->input->post('post_id'));
        echo json_encode($img);
    }

    public function add_custom() {
        $data = $this->input->post();
        $info = array();
        if ($this->getdata->add_custom_val($data)) {
            $info['status'] = 'success';
        } else {
            $info['status'] = 'fail';
        }
        echo json_encode($info);
    }

    public function del_custom() {
        $cid = $this->input->post('cid');
        if ($this->getdata->del_custom_val($cid)) {
            $info['status'] = 'success';
        } else {
            $info['status'] = 'fail';
        }
        echo json_encode($info);
    }

    public function update_custom() {
        $data = $this->input->post();
        $cid = $data['cid'];
        unset($data['cid']);

        $info = array();

        if ($this->getdata->update_custom_val($data, $cid)) {
            $info['status'] = 'success';
            $info['custom'] = $this->getdata->get_custom_val($data['cname']);
        } else {
            $info['status'] = 'fail';
        }
        $info['post_data'] = $this->input->post();
        echo json_encode($info);
    }

    public function trycatch_sms() {
        $tel = $this->input->post('tel');
        $msg = $this->input->post('msg');

        $total_sms = mb_strlen($msg, 'utf-8');
        $total_sms = ceil($total_sms / 70) * count($tel);

        if ($this->getdata->reduce_sms_credit($total_sms)) {
            foreach ($tel as $key => $recv) {
                $res[$key] = $this->sms->send('0000', $recv, $msg);
            }
            $res['status'] = 'success';
            $this->getdata->add_log($tel, "TRYCATCH_SMS_SERVICE", $msg, "SMS");
        } else {
            $res['status'] = 'fail';
            $res['tel'] = $tel;
            $res['msg'] = $msg;
            $res['msg_length'] = mb_strlen($msg, 'utf-8');
            $res['req_credit'] = $total_sms;
        }
        echo json_encode($res);
    }

    public function trycatch_newletter() {
        $mail = $this->input->post('email');
        $msg = $this->input->post('msg');
        $subject = $this->input->post('subject');
        foreach ($mail as $mail_person) {
            $this->email_server->email_send("The Elysium Garden : Newsletter", $subject, null, $msg, array($mail_person));
        }

        $this->getdata->add_log($mail, $subject, $msg, "Email");
        echo json_encode(array('status' => 'success'));
    }

    public function contact_us() {
        $data = $this->input->post();
        $res = $this->email_server->email_send($data['contact-name'], 'Contact from Website', $data['contact-email'], nl2br($data['contact-detail']), $this->email_server->owner);
        echo $res;
    }

    public function regist() {
        $data = $this->input->post();
        $res['status'] = "fail";
        if ($data['password'] === $data['password2']) {
            if (count($this->auth->check_username($data['username'])) == 0) {
                $email = $data['email'];
                $telephone = $data['telephone'];
                $fax = $data['fax'];

                $data['password'] = do_hash($data['password'], 'md5');

                unset($data['email']);
                unset($data['telephone']);
                unset($data['password2']);
                unset($data['fax']);
                if ($this->auth->regist($data)) {
                    $info = $this->auth->check_username($data['username']);
                    $info = $info[0];

                    $this->getdata->add_custom_val(array(
                        'belong_id' => $info->uid,
                        'ctable' => 'users',
                        'cname' => 'email',
                        'cvalue' => $email
                    ));
                    $this->getdata->add_custom_val(array(
                        'belong_id' => $info->uid,
                        'ctable' => 'users',
                        'cname' => 'telephone',
                        'cvalue' => $telephone
                    ));
                    $this->getdata->add_custom_val(array(
                        'belong_id' => $info->uid,
                        'ctable' => 'users',
                        'cname' => 'fax',
                        'cvalue' => $fax
                    ));

                    $res['status'] = 'success';

                    $msg = "<h1>New member request</h1>";
                    $msg .= "<p>{$data['name']} ({$email})</p>";
                    $msg .= "<p>view full request <a href='" . site_url('trycatch/waitinglist') . "'>click here</a> or Quick approve please <a href='" . site_url('api/quick_approve/' . $info->uid) . "'>click here</a></p>";
                    $this->email_server->email_send($data['name'], 'Member request', $email, nl2br($msg), $this->email_server->owner);
                }
            } else {
                $res['response'] = "Username on exist";
            }
        } else {
            $res['response'] = "Password Mismatch";
        }
        echo json_encode($res);
    }

    public function approve_member($uid) {
        $this->quick_approve($uid, false);
    }

    public function quick_approve($uid, $redirect = true) {
        if ($this->auth->approve_member($uid)) {

            $data['status'] = 'success';

            $email = $this->getdata->get_custom_val('email', 'desc', $uid, 'users');
            $email = $email[0];
            $email = $email->cvalue;

            $userinfo = $this->auth->check_uid($uid);
            $userinfo = $userinfo[0];

            $msg = "<h1>Your account has approved</h1>";
            $msg .= "<p><b>username: </b>{$userinfo->username}</p>";
            $msg .= "<p>You can enter your account by <a href='" . site_url() . "'>click here</a></p>";

            $this->email_server->email_send(null, "[The Elysium Garden] Your account has approved", null, $msg, $email);
        } else {
            $data['status'] = 'fail';
        }

        if ($redirect) {
            if ($data['status'] == 'success') {
                redirect(site_url() . "?approve=success");
            } else {
                redirect(site_url() . "?approve=fail");
            }
        } else {
            echo json_encode($data);
        }
    }

    public function update_user_level() {
        $uid = $this->input->post('uid');
        $level = $this->input->post('level');

        if ($this->auth->update_user_level($level, $uid)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function update_order_state() {
        $data = $this->input->post();
        if ($this->getdata->update_order($data['status'], $data['order_id'])) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function email_order_update() {
        $order_id = $this->input->get_post('order_id');
        echo $this->update_with_attatchment($order_id);
    }

    private function update_with_attatchment($order_id) {
        $attach = array();
        $attach_order_bill = $this->convert_id->export_pdf($order_id);
        array_push($attach, $attach_order_bill);
        $this->email_server->email_order($order_id, $new_order = false, $attach);
        $data['status'] = 'success';
        return json_encode($data);
    }

    public function update_profile($uid) {
        $data = $this->input->post();
        if ($this->auth->update_user_info($uid, $data)) {
            $res['status'] = 'success';
        } else {
            $res['status'] = 'fail';
        }
        echo json_encode($res);
    }

    public function set_pay_date() {
//        order_id and due date
        $data = $this->input->post();
        if ($this->getdata->set_credit($data['order_id'], $data['credit_date'])) {
            $info['status'] = "success";
        } else {
            $info['status'] = "fail";
        }
        echo json_encode($info);
    }

    public function update_prefix_code() {
        $data = $this->input->post();
        if ($this->getdata->update_prefix_code($data['uid'], $data['prefix_code'])) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function chage_pass() {
        $data = $this->input->post();
        if ($this->auth->change_password($data)) {
            $res['status'] = 'success';
        } else {
            $res['status'] = 'fail';
        }
        echo json_encode($res);
    }

    public function get_due_notification() {
        $client = $this->getdata->get_due_before();
        $txt = array();
        foreach ($client as &$person) {
            $temp = $this->update_with_attatchment($person->order_id);
            array_push($txt, $temp);
        }
        print_r($txt);
    }

}

?>
