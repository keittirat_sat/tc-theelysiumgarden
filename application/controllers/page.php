<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class page extends CI_Controller {

    private $loged;

    public function __construct() {
        parent::__construct();

        $this->loged = $this->auth->is_loged();
//        $loged = true;
        $this->template->set('loged', $this->loged);
        if ($this->loged) {
            $cart = $this->cart_system->retrive_cart();
            $this->template->set('cart', $cart);
            $user_session = $this->auth->get_user_info();
            $user = $this->auth->check_uid($user_session->uid);
            $this->template->set('user', $user[0]);
        }
    }

    public function index($l = 'en') {
        $this->template->set('title', 'Home | หน้าแรก');
        $this->template->set('vagetable', $this->getdata->get_product_type('vagetable', 4));
        $this->template->set('fruit', $this->getdata->get_product_type('fruit', 4));
        $this->template->set('flower', $this->getdata->get_product_type('flower', 4));
        
        $all_product = array_merge($this->getdata->get_product_type('vagetable', 4),$this->getdata->get_product_type('fruit', 4),$this->getdata->get_product_type('flower', 4));
        
//        $this->template->set('all_product', $this->getdata->get_product_post('product', 12));
        $this->template->set('all_product', $all_product);
        $this->template->set('lang', $l);
        $this->template->load();
    }

    public function product($l = 'en') {
        $this->template->set('title', 'Product | ผลิตภัณฑ์');
        $this->template->set('vegetable', $this->getdata->get_product_type('vagetable'));
        $this->template->set('fruit', $this->getdata->get_product_type('fruit'));
        $this->template->set('flower', $this->getdata->get_product_type('flower'));
        $this->template->set('lang', $l);
        $this->template->load();
    }

    public function gardening($l = 'en') {
        $this->template->set('title', 'Landscaping Service | บริการจัดสวน');
        $this->template->set('gallery', $this->getdata->get_product_post('gallery'));
        $this->template->set('lang', $l);
        $this->template->load();
    }

    public function album($album, $l = 'en') {
        $post = $this->getdata->get_post($album);
        if ($post) {
            $name = json_decode($post->pname);
            $this->template->set('title', "{$name->en} | {$name->th}");
            $this->template->set('album', $this->getdata->get_product_post('gallery'));
            $this->template->set('name', $name);
            $this->template->set('detail', json_decode($post->pdetail));
            $this->template->set('img_list', $post->img_list);
            $this->template->set('all', $post);
            $this->template->set('lang', $l);
//            $js = js_asset('trycatch_parallax.js');
//            $this->template->set('js', $js);
            $this->template->load();
        } else {
            redirect('page/gardening');
        }
    }

    public function faq($l = 'en') {
        $this->template->set('title', "FAQ | คำถามที่พบบ่อย");
        $this->template->set('lang', $l);
        $this->template->load();
    }

    public function contact($l = 'en') {
        $this->template->set('title', "Contact Us | ติดต่อเรา");
        $this->template->set('lang', $l);

        $js = '<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>';
        $js .= js_asset('jquery-ui-1.8.18.min.js');
        $js .= js_asset('jquery.ui.map.js');
//        $js .= js_asset('trycatch_parallax.js');
        $this->template->set('js', $js);

        $this->template->load();
    }

    public function checkout($transaction_id, $l = 'en') {
        if ($this->loged) {
            $user = $this->auth->get_user_info();
            $cart = $this->cart_system->retrive_cart();
            $total = 0;
            if (!empty($cart)) {
                foreach ($cart as $item) {
                    //Sum all price
                    $temp_product = $this->getdata->get_post($item['pid']);
                    $total += $temp_product->price * $item['priece'];

                    //Reduce Stock
                    $this->getdata->reduce_stock($item['pid'], $item['priece']);
                }

                //Strore Order into database
                $this->cart_system->sent_order($user->uid, $total);

                //Get last order_id from database
                $order_from_db = $this->getdata->get_last_order();

                if (!empty($order_from_db)) {
                    //if found it will store the amount from product into database;
                    $order_from_db = $order_from_db[0];
                    foreach ($cart as $item) {
                        //strore order amount by each product
                        $this->getdata->insert_order_item($order_from_db->order_id, $item['pid'], $item['priece']);
                    }
                    //Clear cart
                    $this->cart_system->clear_cart();

                    //Email billing
                    $this->email_order($order_from_db->order_id);

                    $this->template->set('title', "Check out | ประมวลผลรายการ");
                    $this->template->set('lang', $l);
                    $this->template->set('transaction_id', $order_from_db->order_id);
                    $this->template->load();
                }
            } else {
                redirect(site_url());
            }
        } else {
            redirect(site_url());
        }
    }

//    public function checkout($transaction_id, $l = 'en') {
//        if ($this->loged) {
//            $user = $this->auth->get_user_info();
//            $cart = $this->cart_system->retrive_cart();
//
//
//            if (!empty($cart)) {
//                $total = 0;
//
//                foreach ($cart as $item) {
//                    $this->getdata->reduce_stock($item['pid'], $item['priece']);
//                    $total += $item['total'];
//                    $this->getdata->insert_order_item($transaction_id, $item['pid'], $item['priece']);
//                }
//
//                $this->cart_system->sent_order($transaction_id, $user->uid, $total);
//                $this->cart_system->clear_cart();
//
//                $this->email_order($transaction_id);
//
//                $this->template->set('title', "Check out | ประมวลผลรายการ");
//                $this->template->set('lang', $l);
//                $this->template->set('transaction_id', $transaction_id);
//                $this->template->load();
//            } else {
//                redirect(site_url());
//            }
//        }
//    }

    public function email_order($transaction_id) {
        $this->email_server->email_order($transaction_id);
    }

    public function cart_template($lang = 'en') {
        $cart = $this->cart_system->retrive_cart();
        echo $this->template->element('cart_template', array('cart' => $cart, 'lang' => $lang));
    }

    public function test_pdf($transaction_id) {
        $this->load->library('mpdf');
        $this->mpdf->mpdf('th', '', 0, '', 5, 5, 5, 0, 0, 0, 'P');
        $order = $this->getdata->get_order_by_id($transaction_id);
        $order_user = $this->auth->check_uid($order->uid);
        $order_item = $this->getdata->get_order_list($order->order_id);
        $msg = $this->template->element('order_pdf', array('order' => $order, 'order_user' => $order_user, 'order_item' => $order_item, 'can_edit' => false));
        $this->mpdf->WriteHTML($msg);
        $this->mpdf->SetAutoFont(AUTOFONT_THAIVIET);
        $this->mpdf->Output();
        $this->mpdf->close();
    }

    public function zero() {
        echo "<pre>" . print_r($this->auth->get_user_info(), true) . "</pre>";
    }

}

?>
