<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Trycatch extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //Login
        if (!$this->auth->is_loged('admin')) {
            $this->template->set('title', 'Please login');
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch/welcome'));
        }
    }

    public function welcome() {
        //Welcome Page        
        if ($this->auth->is_loged('admin')) {
//            $this->template->set('title', 'Welcome to The ElysiumGarden');
//            $this->load->library('site_analytic');
//            $this->template->set('visitor_pageview', $this->site_analytic->get_visitor_pageview());
//            $this->template->set('keyword', $this->site_analytic->get_keyword());
//            $this->template->set('site_referrer', $this->site_analytic->get_user_referrer());
//            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
//            $this->template->load(null, 'admin');
            redirect(site_url('trycatch/product'));
        } else {
            redirect(site_url('trycatch'));
        }
    }

    /*
     * Product Section
     */

    public function product() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', 'All Product');
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('product', $this->getdata->get_product_post());
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function new_product() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', 'New Product');
            $this->template->set('post_id', date('U'));
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function post() {
        if ($this->auth->is_loged('admin')) {
            $post_id = $this->input->get_post('post_id');
            $post = $this->getdata->get_post($post_id);

            $pname = json_decode($post->pname);
            $pdetail = json_decode($post->pdetail);

            $this->template->set('title', "{$pname->en} | {$pname->th}");
            $this->template->set('pname_en', $pname->en);
            $this->template->set('pname_th', $pname->th);
            $this->template->set('pdetail_en', $pdetail->en);
            $this->template->set('pdetail_th', $pdetail->th);
            $this->template->set('pquantity', $post->pquantity);
            $this->template->set('pthumbnail', $post->pthumbnail);
            $this->template->set('price', $post->price);
            $this->template->set('img_url', $post->img_url);
            $this->template->set('post_id', $post->pid);
            $this->template->set('post_type', $post->post_type);
            $this->template->set('cate', $post->category);
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    /*
     * Gallery
     */

    public function gallery() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', 'All Gallery');
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('post', $this->getdata->get_product_post('gallery'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function add_gallery() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Add new gallery");
            $this->template->set('post_id', date('U'));
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function gallery_edit() {
        if ($this->auth->is_loged('admin')) {
            $post_id = $this->input->get_post('post_id');
            $post = $this->getdata->get_post($post_id);

            $pname = json_decode($post->pname);
            $pdetail = json_decode($post->pdetail);

            $this->template->set('title', "{$pname->en} | {$pname->th}");
            $this->template->set('pname_en', $pname->en);
            $this->template->set('pname_th', $pname->th);
            $this->template->set('pdetail_en', $pdetail->en);
            $this->template->set('pdetail_th', $pdetail->th);
            $this->template->set('pquantity', $post->pquantity);
            $this->template->set('pthumbnail', $post->pthumbnail);
            $this->template->set('price', $post->price);
            $this->template->set('img_url', $post->img_url);
            $this->template->set('post_id', $post->pid);
            $this->template->set('post_type', $post->post_type);

            //Img List
            $this->template->set('img_list', $post->img_list);

            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function email() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Email list");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('custom', $this->getdata->get_custom_val('email'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function telephone() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Telephone list");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('custom', $this->getdata->get_custom_val('telephone'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function sms() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Send SMS");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('custom', $this->getdata->get_custom_val('telephone'));
            $this->template->set('sms_credit', $this->getdata->get_custom_val('sms_credit'));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }
    
    public function newsletter(){
        if ($this->auth->is_loged('admin')) {
            $js = '<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>';
            $this->template->set('title', "Newsletter");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('custom', $this->getdata->get_custom_val('email'));
            $this->template->set('js', $js);
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function member() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "All Memeber");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('userlist', $this->auth->get_approve_user_list());
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function waitinglist() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Waiting list member");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('userlist', $this->auth->get_waiting_user_list());
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function banned() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Banned Memeber");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('userlist', $this->auth->get_user_by_level(2));
            $this->template->load('trycatch/member', 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function order() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "New comming order");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('order', $this->getdata->get_type_order(0));
            $this->template->load(null, 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function process() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Order on processing");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('order', $this->getdata->get_type_order(1));
            $this->template->load('trycatch/order', 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function credit() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Credit billing");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('order', $this->getdata->get_type_order(2));
            $this->template->load('trycatch/order', 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function paid() {
        if ($this->auth->is_loged('admin')) {
            $this->template->set('title', "Already paid");
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
            $this->template->set('order', $this->getdata->get_type_order(3));
            $this->template->load('trycatch/order', 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function view_order($order_id) {
        if ($this->auth->is_loged('admin')) {
            $order = $this->getdata->get_order_by_id($order_id);
            $this->template->set('title', "Order ID : ".$this->convert_id->order_id($order->order_id));
            $this->template->set('sidebar', $this->template->element('trycatch/sidebar'));
//            $this->template->set('order', $order);
//            $this->template->set('order_user', $this->auth->check_uid($order->uid));
//            $this->template->set('order_item', $this->getdata->get_order_list($order->order_id));


            $order = $this->getdata->get_order_by_id($order_id);
            $order_user = $this->auth->check_uid($order->uid);
            $order_item = $this->getdata->get_order_list($order->order_id);
            $msg = $this->template->element('email_template', array('order' => $order, 'order_user' => $order_user, 'order_item' => $order_item, 'can_edit' => true));
            $this->template->set('order_info', $msg);

            $this->template->load('trycatch/view_order2', 'admin');
        } else {
            redirect(site_url('trycatch'));
        }
    }

    public function debug() {
        $str = $this->session->all_userdata();
        echo "<pre>" . print_r($str, true) . "</pre>";

        $cart = $this->session->userdata('cart');
        echo "Cart<br/><pre>" . print_r($cart, true) . "</pre>";
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */