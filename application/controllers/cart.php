<?php

class Cart extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain; charset=utf-8");
        header('Access-Control-Allow-Origin: *');
    }

    public function add_cart() {
        $param = $this->input->post();
        $log = $this->cart_system->add_cart($param['pid'], $param['total']);
        echo json_encode($log);
    }
    
    public function get_cart(){
        $cart = $this->cart_system->retrive_cart();
        echo json_encode($cart);
    }

}

?>
